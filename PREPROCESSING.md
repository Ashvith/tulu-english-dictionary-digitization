# Preprocessing

1. Extract scanned data from PDF using Poppler
```sh
pdftoppm ../tulu-english-dictionary-by-m-mariappa-bhat-and-a-shankar-kedilya-university-of-madras-text.pdf -png image
```

2. Use Tesseract to extract OCR
```sh
for FILE in ../raw_images/*
do
    tesseract $FILE ${FILE#../raw_images/} -l eng+kan
done
```
