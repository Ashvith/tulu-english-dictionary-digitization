# Tulu-English Dictionary Digitization

## About

This project aims to digitize the Tulu-English dictionary by M. Mariappa Bhat and A. Shankar Kedilya as an effort to promote and preserve the Tulu language. Alternatively, older dictionaries by Rev. A. Maner may also be digitized to create a two-way dictionary.

## Intuition

Currently the three available dictionaries (two Tulu-English dictionaries and one English-Tulu dictionary) are out of print, and also not available in their electronic format. Although there are online dictionaries out there, the data is locked behind some unknown server with no way to access them publically, in a convenient manner. In the coming future, where low-resource languages might benefit from technological advancement of language models, having such data handy will prove to be beneficial to the preservation of language.

## Goals

- [ ] Digitize the Tulu-English dictionary by M. Mariappa Bhat and A. Shankar Kedilya.
- [ ] Digitize Rev. A. Maner's Tulu-English and English-Tulu dictionary to allow two-way reading.
- [ ] Convert from Kannada to Tulu lipi.
- [ ] Create a new dictionary based on both the dictionary, and add growing set of words, including previously left-out and borrowed words.
- [ ] Create an open-source dataset with extra information, such as source of origin, dialect type, etc.

## Donate

If this project may benefit you, I'd appreciate it if you are consider to supporting me by sharing a reasonable part of the grant available.

