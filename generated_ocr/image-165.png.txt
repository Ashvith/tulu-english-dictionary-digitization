ಪಂಚಕುರ್ಷೆ. paficakurve

dimyofte paitanigo

as an offenng to ೩ deity. ಪಂಚಾಳೆಂ್‌ | ೧ ಗಂ alery. ೫ the ﬁve

[pafica+kajjayo.]

ಸಂಚಕುರ್ವೆ paficakurve. 1. ೩ spe- '
cial basket. [pafica+kurve.] |
ಸಂಚಗವ್ಯೊ paficagavyo. 1. a mix- |
ture of five articles derived |

from the cow (milk, curds,
ghee, urine and cow-dung)

and taken ೩5 ೩ means of |

purification. [pafica+
gavyo.]
ಪಂಚಸಕೀರೆ paficapakiré. 1. ೩
pauper. [pafica+pakiré.]
ಪಂಚಪಶ್ರೆ paficapatre. ೫. the

worm-wood plant. [pafica+ |

patre.]

ಪಂಚಪಾತ್ರೆ paficapatre. 9. ೩ small |

metal tumbler used for fill-
ing water required for
ablutions. [pafica{-patre.}

ಪಂಚವಳ್ಳಿ ಬಚ್ಚಿಕೆ paficavalli bacci-
re. 1೩ also ಪಂಚೋಳಿ ೩ very
pungent kind of betel-leaf.
[paiicavalli+baccire.]

ಪಂಚಾಂಗೊ paficango. 9. the
almanac ; the foundation of
a building.

ಸಂಚಾತಿಕೆ 7೩೫೦೫/1100. 11. also ಪಂ
ಚಾಯಿತಿ arbitration; delibera-
tions, talking.

ಸಂಚಾಮೃತೊ paficamrito. 11. the |

toddy given to the devil-
dancer (special meaning).
ಪಂಚಾಯಿತಿ paficayiti. 9. 5೦೮ ಪಂಚಾ
ತಿಕೆ; an assembly composed
of five or more persons to
settle a matter.
ಸಂಚಾಳಿ paficali. #. talkative

man [M].

| ಪಂಜಿ paiiji. #. ೩ pig.

classes of artificers: gold-
smiths, blacksmiths, carp-
enters, braziers and masons.
[panca+-3jery ?]

ಸಂಬೇರ್‌ paiicery. 11. one-cighth
of a maund, three and half
pounds.

ಪಂಚೋಳಿ paficoli. 11. 506 ಪಂಚವಳ್ಳಿ
ಬಚ್ಚಿರೆ.

ಪಂಜ 7೩7]೩. 11. scarcity, famine
[21].

—giravi
epilepsy, —betta ೩ kind of
rattan, —bedry ೩ kind of
bamboo, ey— porcupine,
mullu— porcupine.

ಪಂಜುರ್ಲಿ pafijurli. 1. name for
a demon.

| ಪಟಿಸಟ pata-pata. . a flapping

or rattling sound.

ಪಟಲ patala. 7. ೩150 ಪಟ್ಟಿ a mode
of late cultivation.

ಪಟಾಕಿ pataki. 11. ೩ cracker.

ಪಟಾಕಿಲೊ patakilo. 11. ೩150 ಪಟ್ಟಿ
ಕಾಯಿ snake-gourd.

ಪಟಕಾರೊ patikaro. 11- alum.

ಪಕೆ patike. 1. also ಪಟ್ಟಿ, ಪಡಿಕೆ
urine; wickedness.

ಪಟಿಂಗೆ patingé. #. ೩ rogue.

ನಟೀಲೆ patelé. 1೬. headman of a
village.

ಪಟ್ಟಿ patke. 11. see ಪಡಿಕೆ, edde—
the good and the bad.

ಪಟ್ಟ pattu. 91. ೩ nest [M].

ಪಟ್ಟ್ಸ್‌ pattu. ೪. 10 distribute.

ಸಟ್ಟಿಡಿ pattadi. 2. ೩ 1176 08
necklace [11].

ಪಟ್ಟಾಂಗೊ pattango. 1). 1616 talk,
