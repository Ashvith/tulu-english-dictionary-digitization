ಪವನೊ pavarno

150

ಪಾಜಿವು-೧೫]6೪॥

ಪವನೊ pavano. #. a 56176, ೩ !
strainer [M].

ಪವುಂಚೆ pavufici. 1೬. ೩ bracelet
08 8016 beads [M].

ಪವುಂಜಿ pavufiji. n. ೩ Dasara
festival.

ಪಶೆಂಗ್ರಿ pasengri. #. ೬೬೦ ಪೆಸೆಂಗ್ರಿ
a kind of savoury 5106-6151,

ಪಸಂದೊ pasando. adv. comely,
handsome, nice.

ಪಸರ್‌' pasary. ೪. 10 extend,
diffuse, spread.

ಪಸಿ pasi. 7. boy, child [M].

ಪಸುಂಜೆ pasumbe. ೫. ೩ sack or
bag made of coir or any
rough yarn.

ಪಸೆ pase. #. moisture, wetness, |
dampness; greasiness, oili-
ness.

ಪಸೆಂಗ್ರಿ pasengri. #. 566 ಪಶೆಂಗ್ರಿ.

ಪಸೆರು paseru. ೫. 10 spread!
manure etc.

ಪಸ್ಥಿ paski. 1). also ಬಸ್ಥಿ ೩ 17066 |

. 0? punishment.

ಪಸ್ಸ್‌ passy. ೪. 566 ಪರ್ಶ್ಸ್‌.

ಷಳಂಗ್‌ pajangy. ೫. 566 ಪರಂಗ್‌. |

ಪಳದ್ಯೊ pajadyo. #. soup ೦೯ |
pepper-water.

ಪಳಪಳ pajapala. adv. rippling |
noise of water or a term to

.. indicate glittering.

ಪಳಯೆ palayé. 7. 566 ಪರಯೆ. |

ಪಳ್ಳಿ, pali. 11. 566 ಸರ್ದಿ,

. 89, pali. n. see ಸಲಿ. |

ಫಳುಕು pajuku. ೪. ೩150 ಷಳ್ಳು 10 '
be soft, 10 0೮ flexible (due
to slenderness). |
ಪಳೆಂಕ್‌ pajenku. ೪. (0 1538 |
overturn. A

'`ಪಾಂಗಳ pangala.

ಪಳ್ಳು palku. v. 566 ಪಳುಕು.

. ಪಳ್ನೀರ್‌ 0೩11117. 91. 566 ಪನ್ನೀರ್‌,

ಪಳ್ಳ 2೩11೩. 11. ೩ shallow pond,
a depressed place where
rain water collects itself.
prv.pallady kulludu paraniu
pattiyé sitting in the pit, he
caught young frogs.

ಹಳ್ಳಿ palli. #. ೩ mosque.

®Y, pajlu. 11. jackal.

ಪಳ್ಳೆ ನ pallena. 11. light, shine.

ಪಾಕ್‌ paku. adv. once a day[M].

ಪಾಕೊ pako. adj. many.

ಪಾಗರೊ pagaro. . also ಪಾಗಾಕೊ
an enclosure, a rampart.

| ಪಾಗಾರೊ pagaro. 11. 566 ಪಾಗರೊ,

ಪಾಂಕ್‌ panku. 1೩. ೩ kind 01
musical instrument.

ಪಾಂಗ್‌ pangu. v. to despise,
mock at, jeer [M],

ಪಾಂಗ್‌ pangy. 1- share [M].

meﬁm& ಯ panganpaya. 1. a
family name of Tuluva
Brahmins. [panganna+-aya.]

7. ೩ portion,
share [M[.

ಪಾಚೆ paca. #. lungs of fish,
skin of the leg.

ಪಾಚ್ಕೆ paci. #. also ಪಾಂಬ ಬ
ಪಾಮಾಚಿ MOSS.

ಪಾಚ್ಚಿ paci. 1. 50076.

ಪಾಚಿಲ್‌ pacily. #. ೩ bi-valved
shell.

ಪಾಚೊಲ್ರ್ಯಾರು pacoltyaru. 1. a
kind of fish.

ಪಾಜೆ paje. 1೬. also ಪಾಜೆಯಿ algae,
lichen.

ಪಾಜೆಯಿ. ॥8]6]1/. 14. see ಪಾಜೆೆ

ಪಾಜೆವು pajevu. 11. also ನಾಜೋವು

