ಕೈಮ kaima

ಕೈಮ kaima. 1. 2 b

i?‘s:bd kaimani. 7. ೩ hand bell
used during worship. [kai+4
mani.]

ಕೈಮಣ್ಣ್‌ 1811701200. 1. 1176
earth used ೩5 ೩ plaster.
[kai--mannu.]

ಕೈಮರ್ಜೆಲ್‌ kaimarcelu. 1.
domestication; taming. [kai
—“+marceju.]

ಕೈಮರ್ದ್‌ kaimardy. #. secret
medicine supposed to bring
a person under one’s control
after its administration.
kai+mardy.]

ಕೈಮಾಡ kaimada. 11. ೩ funeral
ceremony. [kai+mada ?]

ಕೈನಾಸೊ kaimaso. n. a slow
poison. [kai+maso? ]

ಕೈನುಟ್ಟು kaimuttu. 11. urgent
need. [kai+muttu.]

ಕೈಮೆತ್ತೆ kaimette. 11. a kind of |

musical instrument. [1001--
mette.]

ಕೈಯಾರ್‌, kaiyarg. 11. a small
stream. [kai4-ary.]

ಕೈಯಾರ kaiyary. ೫.
name.

ಕೈಯಾರೊ kaiyaro. adv. with
one’s own hand. [kai4-aro ?]

ಕೈಯೊಲಿ kaiyoli. #. the mouth
of a weel.

ಕೈಯೊಳೆ kaiyole. #. the dug-out
water course by the side of
the field. [kai+-poje ?]

ಕೈಲ್‌, kaily. 2. a spoon, ladle;
the handle of a hoe.

ಕೈಲ್‌ kaily. 11. the whole bunch
or cluster of fruits especially

a place |

ಕೊಕ್ಕುಕೆ kokkare

of palms such as plantam.

palmyra etc.

ಕೈಲ್‌ ತಾರಿ kailutari. #. ೩ feminine
species of palmyra. [kailu+
tari.]

ಕೈಲಕಣ್ಕೆ kailakade. 11. handle
of a ladle.

ಕೈಲಕಷ್ಟೆ kailakade. 11. a kind of
fish.

ಕೈಲದೈ, kailade. 1. a kind of
cake. [kai+adye ?]

8,08, kailade. #. ೩ pigeon hole.
[kai+-ade.]

ಕೈಲಪುರ್ಲಿ kailapurli. ೫%. see
ಕಾಯೆರ್‌ಪುಳಿ,

ಕೈವುಳಿ kaivuli. 11, ೩ small chisel.
[kai+-uli.]

ಕೈವೊದಕ್‌ kaivodaky.#. progress,
prosperity. [kai+vodaku?]

ಕೈವೊಸ್ಪಿಗೆ kaivoppige. #. ೩ signa-
ture. [kai--oppige.]

ಕೈಸಟ್ಟಿ kaisatti. #. a round
wooden trough with a
handle. [kai-+satti?]

ಕೈಸಾಲೊ kaisalo. #. ೩ small or
temporary loan without a
pledge. [kai+salo.]

ಕೈಸೂಂಜೆ kaisimbe. 1.
ಕೈತೂಂಬು [1081-1-587706.]

ಕೈಹಾಳಿತೊ kaihalito. 11. estimate
by hand. [kai+halito.]

ಕೊಕ್ಳು kokka. 11. the game of
hide and seek.

ಕೊಕ್ಟುರ್‌ kokkary. #. roughness.

ಕೊಕ್ಕರ್‌ಕುಳ್ಳು kokkaryku]lu. ೪.
to sit on half bended knees.
[kokkaru+kukju.]

ಕೊಕ್ಕುಕೆ kokkare. 11. Sea Bream

fish.

566

N
