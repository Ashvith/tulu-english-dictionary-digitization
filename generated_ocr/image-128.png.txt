‘

ಡೆಮಾಮಿ damami 106 3, ta

ಡಮಾನಿ damami. ( a hollow | ಜಿಮ್ಮೆ demme. 60). 566 ಎಡಮೆ,
500736; false appearance. ಡೇರೆ dere. 1% tent.
B, damma. 7. ೩ term used | ಡೊಗ್ಗು doggu. v. also ಜೊಗ್ಗು to

to express swift action. kneel. '
ಡಂಬ damba. 7. vanity, show. | ಡೊಂಕು donku. adj. crooked. \
[Sk. dambha]. s ಡೊಣ್ಣೆ (0706. #. cudgel, club.

ಡಾಂಬಿಕೆ dambiké. . pedant, : ಡೊಂಬಿ dombi. 1. rioting.
one given to affectation; a | ದೊಂಬೆ dombé. 7. also ದೊಂಬೆ
hypocrite. a man who performs circus
ಡಿಬ್‌' dib. 1೩. the noise of ೩ feats; the name of a caste.
violent blow (usually with | ಡೊಳ್ಳು. (0113. adj. also ಜೊಲ್ಲು

fist). fat, 56006 ; hollow. — bajiji
ಡಿಬ್ಬು dibbu. 1. kick with leg pot belly.

(usually a foot ball). ಜೋಲಿ doli. 7. also ಜೋಲಿ litter.
Bon dengu. ೪. 566 ಅಡೆಂಗ್‌. Antes dolu. 7. ೩150 ಜೋಲು large
ಡೆಂಜಿ defiji. 7. 566 ಎಡೆಂಜಿ. drum.
ಡೆಣ್ಣ ಡೆಣ್ಣ denpa denna. #. see | ಡೌಲು daulu. ೫. ೩150 ದೌಲು

ಹಣ್ಣ €9, | appearance, spectacular
@3, denna. ೫. 560 ಡಣ್ಣಣ. | 6617687018.

|
£

- na. also -ನಿ and -8 a suffix | convey the meaning of ‘if’.
employed to convert verbs 7 sapta if you see.

into gerunds. ೫ paferupa | -ಂಡ್‌ -ndu. ೩ suffix added to
1816 6006 ijji your talking | verbsintheneuter presentor
is no good. past tense. avu barpundu it

_೦ಟಿ pta. also -ಂಡ a suffix | comes, 001 battypdyit came. I
added on to verbal bases to | -o@ -nda. see -ob.

3

3, 1೩, ೩ letter of the alphabet. -8, -ta. also ದೈ genitive case
-3, -ta, also @, particle indi- | suffix in the singular.
cating past tense of a verb. marata of the tree.
811% korte 8 gave. -3, ta. a syllable occurring

