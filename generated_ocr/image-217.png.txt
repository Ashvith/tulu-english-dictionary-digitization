ಮಸೆಲ್‌`7785211.'

295

.

s, iy

- dissatisfaction, etc. [Sk.
smasana.]

ಮಸಲ್‌ masaly. 2. ೬೦. ೦೫೩7೫೯6
colour, 10 10600876 muddy.

ಮಸಲುನಿ masaluni. 7. exu-
berant growth of plants.

ಮಸಾಲೆ masile. 9. curry powder;
horse medicine.

%% masky. ೫. see ಮಸಕ

ಮಸ್ಯೃತಿ maskati. #. fine kind 01
rice.

ಮಸ್ತ್‌, masty. adj. abundant,
plentiful.

ಮಸ್‌ masty. #. also ಮಸಿ fat,
stout arrogance.

ಮಸ್ತಿ masti. 7. see ಮಸ್ತ್ವ್‌

ಮಳಿಗೆ malige. #. ೩ cloth shop.

ಮಳೆ male. 1. 560 ಮರೆ.

ಮಳ್ಪು 17810. ೪. 566 ಆಂಪು,

ಮಳ್ಳೆ malle. 1. the outer-shell
covering of cashew-nut.

ಮಾಕಟೈೈ mikate. 1. a deceitful
woman.

ಮಾಸಟ್ಕೆ mikate. adj. deceitful,
fraudulent.

ಮಾಕಲಿ mikali. 1. notches made
at the end of the muatney so
as to tie it to  the yoke
without slipping.

ಮಾಗಣೆ 1778126. 1. ೩ division
“of ೩ 18100.

ಮಾಗಯಿ magayi. ೫. an ear-
ornament.

ಮಾಗು magu. ೪. 10 ripen.

ಮಾಂಕಾಳಿ. 778/1011. 1. 066655
Durga. [Sk. mahakali.]

ಮಾಂಗು mangu. 2. also :bam to
disappear, Vanish.

ಮಾಜು maju. v. see ಸಾಗು:

ಮಾಂಜಿ maiiji. ೫. " ೩ 1100, of
fish.

ಮಾಟಿ mite. #. 11016, cavxty

ಮಾಟೊ mito. #. sorcery, wnch-
craft.

ಮಾಢ್‌ mady. #. the roof. anta-
ru—, kalla—, pakki—,
pucce— are different vari-
eties of roof.

ಮಾಡಂತಿಲ್ಲಾಯ madantilliya. 1೩.
a. family name of Tuluva

Brahmins. ‘[madantilla+
aya.]

ಮಾಡಾವು madavu. ೪. to cultivate
as land [M].

ಮಾಡು 77161. ೫. ೩ weapon made
of ೩ deer’s horn.

ಮಾಡೊ 77800. #. shrine ofa
demon.

ಮಾಣಾವು 17581. ``. to lull to
sleep, to 165567, to decrease.

ಮಾಣಿ mani. #. ೩ Brahmin boy
[Sk. manavaka.]

ಮಾಣಿಗೊ minigo. #.:name of a
female person.

ಮಾಣ್ಯೆ magye. ೫. ೩ ಗೆಜೆಣಾ0೫
priest.

ಮಾತ್‌ maty. ೨. also ಮಾಸ್ಕ್‌ to
pour.

ಮಾತ್ಮ 778/೩, n. see ಮಂತ. -

ಮಾತ mita. #. the exterior
suie

ಮಾತಿರಿ mitiri. 1. ebb.

ಮಾತು mitu. v. also ಮಾಪು to
lounge about, to saunter;
to change, as clothes.

ಮಾದ್ಮ್‌ wady. v. to turn about:*

ಮಾದ್ಮ್‌ mady. 1. turning about
೫ರ
