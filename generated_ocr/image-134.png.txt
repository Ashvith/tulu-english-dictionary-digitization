ತಾಬುಲು tabulu

272

At

ಶಾಬುಲು tabulu. #. ೩ tinsel; the
cuts as of ೩ precious stone.
— kaji an artistic bangle.

ಶಾಮರೆ tamare. 11. lotus.

ಶಾಮರೆ ರೋಗೊ tamare 16g0. ೫. a
wasting disease in children;
green sickness.

ತಾಮಸೊ tamaso. 11. late. [Sk.
tamasa.] [dish.

ಶಾಂಬಾಣೊ tambano.#.a metallic

ತಾಂಬ್ರೊ tambro. 1. copper.

ತಾಯಿಗಂಡೆ tayigande. #. ೩ rogue,
೩ cheat.

ತಾರ್‌, tary. ೫. 566 ಆರ್ಕ್‌.

ತಾರ್‌ tary. ೨. ೩150 ತಾಳ್ಮ್‌ 10 drive
in, dig deep.

ಶಾರ್ಕ್‌ taru. #. also ಹಾರ್‌ width
as of a cloth.

ತಾರ್‌ tary. ೪. also ತಾಳ್ಕ್‌ to fall
down.

ತಾರ್‌ಮಾರ್‌ tarymary. 1. dis-
order, confusion.

ಶಾರಗೆ tarage. 7. 566 ಚಾರಗೆ,

ತಾರಾಯಿ tardyi. ೫. 566 ಚಾರಗೆ.
%ಂ॥6 - dried ೦೦07೩, kori—
a large hard cocoanut,

. gende— ೩ red cocoanut (cf.
gendali), gotu— dried copra,
divita— ೩ cocoanut of the
Laccadive Isles. rankada —
a large cocoanut.

ತಾರಿ tari. ೫. also ತಾಳಿ palmyra.

ತಾರೆ tare. 1. see ಚಾರೆ. [೩ 6.

ತಾರೊ (57೦,೫. two,pies (016 coin-

ಶಾರ್ಕಣೆ tarkane. ೫. proof,
demonstration;  composi-
tion.

ತಾರ್ಕೊಲು tarkolu.
ತರ್ಕೊೋಲ್ಕು.

೫. 566

ತಾಲಂಕಣಿ talankani. 11. ೩150 ತಾಳಂ.
ಕಣಿ ೩ ೫7೦೦6೮೧ peg by which
a water-lift is fastened 10
the stem; a wooden bolt
for a door.

ತಾಲ್ಮಿ tali. 11. a sacred token of
marriage usually made of
gold, tied to the bride by
the bridegroom at the aus-
picious time of marriage.

ತಾಲ್ಕಿ tali. #. an earthen cook-
ing vessel. [Sk. sthali.]

ತಾಲೈ talyé. 1. also ಸಾರ್ಯೈ a
weaver.

ತಾಲೈ talye. 1. ೩150 ಸಾಲ್ಯೈೆ ೩
5016.

ತಾವು tavu. ೫೩. 566 ತಾಮು.

ತಾಸಮ್ಮಿ tasami. 1). also ದಾಸಮ್ಮಿ,
ಸಾಸಮ್ಮಿ mustard.

ತಾಸಮ್ಮಿ tasami. 9. also ದಾಸಮ್ಮಿ,
ಸಾಸಮ್ಮಿ a sweet variety 0!
curry.

ತಾಸೆ tase. #. ೩ kind of drum.
as 17 ತಾಸೆ ಸಮ್ಮೇಳನ.

ತಾಳ್ಕ್‌ 1810. ೪. 10 endure.

ತಾಳ್‌ 1811. ೪. 566 ತಾರ್‌,

ತಾಳ್ಯ 151. ೪. 566 ತಾರ್ಮ್‌.

ತಾಳಂಕಣಿ tajankani. #. 566 ತಾಲಂ
ಕಣಿ,

ತಾಳಿ (511, 11. 566 ತಾರಿ.

ತಾಳಿತ್ತ್ವಾಯ talittaya. 1೩ ೩ family
name 08 Tuluva Brahmins.
[ta)ittat-aya.]

ತಾಳೆ 1516. 11. 566 ಚಾಕೆ.

ತಾಳೊ tajo. #. ೩ cymbal, beat-
ing time in music. [Sk.tala.]

ತಾಳ್ಪು 1810. ೪. see ತಲ್ಪು.

ಎತ 11, also -® ೩ suffix used to
convert masculine nouns
