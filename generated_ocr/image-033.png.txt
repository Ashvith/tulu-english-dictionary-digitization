ಅಮ್ಮ amma 2%

ಅರಲೋಲೆ aralsle

ಅಮ್ಮ amma. 17066 ೦8 calling
the father.

33,8y ammakku. 91. name of
೩ female person. [amma--
[akku.]

ಅಮ್ಮಣ್ಣಿ ammanni. #. name of

a female person. [amma-+- |

anni ?]

ಅಮ್ಮು ammu. 7.
female person.

ಅನ್ಮೈ ammé. 11. father.

ಆಯಿತೊ ayito. 7. also ಆಯಿತೊ
ornament; {readiness.

ST, ayiny. . five.

£, ayinu. pron. ೩೦೦. case
of ಅವು meaning that.

ಆಯ್ಯ ayya. n. ೩ priest, teacher;
೩ ೫650601816 term of add-
ressing ೩ superior.

ಆಯ್ಯಪ್ಪ ayyappa. int. word 10
express pain. [ayya--appa.]
ಆಯ್ಯಪ್ಪ | ayyappu. 11. name

name of a

of a male person. [ayya--
appu.]

e;‘oﬂaﬁn ayyalo. #. five seers
(measures).

ಅಯ್ಯೋ ayyo. 11. particle to
express grief.

ಅರಕ್‌ araku. #. arrack, country
spirit ; lac, sealing wax.

ಅರಕೆ arake. 10). half filled (as
opposed to nilike —full).

ಅರಗ” aragu. ೪. also ಎರಗ” I 10
melt; to agree with the sto-
mach or the system.

ಅರಗಣಿ aragani. 1. kitchen uten-
sils.

ಅರಟು aratu. ೪. 10 make a pecu-
-liar noise.

ಅರಟಿ arate. 1. also ಹರಟಿ 2388,
blabber.

ಅರಣೆ arane. 1 ೩ green lizard.

| ಅರಣೆಬೂರು aranebaru. #. ೩ kind

of creeper. [arane+-biru.]

ಅರಣೆಮಿನಾನ್‌ araneminy. #- a kind
of fish. [arane4+minu.] _

ಅರಂಟ್‌' aranty. ೪. to grope,
search for. G = |

ಅರಂಟೀಲೆ arantelé. 1. ೩150 ಅರೆಂಟಿಕೆ
one looking 80: something,
a greedy person.

ಅರತೆ arate. . heat, burning[M].

ಅರದಳೊ aradalo. 11. also ಅರ್ದಾಳೊ.
yellow arsenic, - ೩ pigment.

ಆರಂತಣೆ arantade. 1. ೩ royal
halt; ೩ palace. [aram--tade:]
cf.-arasu.

ಅರಪು arapu. 2. to cool.-

ಅರಪೆ arape. #. a crack, splif.

ಅರಬಾಯಿ arabayi. . also ಅರೆಬಾಯಿ.
lamentation, loud cry.

ಅರಬಿ arabi. 7. also ಅರ್ಬಿ a water-
fall. »

ಅರಬು ೩೯೩೬. 1೩
greed. ` 2

ಅರಮಡಲ್‌ aramadaly. 1. “also
ಅರೆಮುಡಲ್‌' the feathery bra-
nch of palmyra. [ara+ma-
daly.] g

ಅರಮನೆ aramane.
[ara+mane.]

ಅರಲ್ಕ್‌ araly. - 1.
drizzling rain.

ಅರಲ್ಕ್‌ araly. ೪.
open. - -

ಅರಲೋಲೆ aralole. 7. a-kind of
ear ornament, a: tender
palmleaf. [aral+-ole.} .=

covetousness,

1ಬ ಇ ೩08.
also ಇರಿಲ್‌

10 ಗಾ
