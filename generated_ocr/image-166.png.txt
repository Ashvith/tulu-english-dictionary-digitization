ಪಟ್ಟೆ patte

ಪಟ್ಟೆ patte. 7. ೩ strip; silk;
document specifying a
grant of land; a strip of a
palm-tree; a belt of metal
or leather; cart-tyre.

ಪಟ್ಟೊ patto. 7. crowning, ins-
tallation; throne, rank,
title.

ಪಟ್ಲಿ patla. 2. see ಪಟಿಲ.

ಪಟ್ಟಿ ಕಾಯಿ patla-kayi. 11. see ಪಟಾ
ಕಿಲೊ.

ಪಟ್ಲಾಮು patlamu. 11. regiment,

ಪಡ pada. 1 word indicating
left direction while driving
animals in ploughing.

ಪಡಪೋಸು padaposu. 10). useless;
shallow.

ಪಡಂಜೋಳಿ padamboli. 1. a liar.

ಪಡಯ padaya. adv. ೦೧೮ upon
another [M].

ಪಡೆರ್‌ padary. ೪. 10 creep.

ಪಡವು padavu. 1. ೩ ೪5861.

144

ನಣ್ಯೆ pane

ಪಡುಂಬು padumbu. 1. ೩ rough
canvas cloth [M].

gty o8 paduvatniya. ೫. ೩
family name of Tuluva
Brahmins.

@3, pade. #. multitude, mob;
army.

@8, pade. . to get, to acquire,

ಪಡ್ಚ padca 11. finish, settlement.

ಪಡ್ಡಣೆ paddane. adj. sloping,
flat.

| ಪಡ್ಜಾಯಿ paddayi. 11. also 3

the west.
ಪಡ್ಡಿಲ್ಲಾಯ paddillaya. 1-೩
family name of Tuluva
Brahmins. [paddlly-+iya.]
ಸಡ್ಜೆಯಿ paddeyi. 11. 566 ಸಡ್ಡಾಯಿ.
ಪಡ್ಪು padpu. 11. ೩ plain, ೩ waste-
ground.

| ಪಡ್ಪೆ padpe. 7. see ಅಡಜಿ,

ಪಡಸಾಲೆ padasale. 11. a verandah. |

ಪಡಾಯಿ padayi. 11. trouble.

ಪೆಡಾವು padiavu. 1. profit, gain.

ಪಡಿ padi. 11- allowance of food;
rations; a small weight; a
thrashing frame; ೩ door or
shutter made of slips of
bamboo.

ಪಡಿಕೆ padike. 7. 566 BB

ಪಡಿತಡೆ paditade. 1. temporary
building 111].

ಪಡಿತೊ padito. 11. a stripe; mark
of a whip.

ಪಡಿವಳ padivala. 11. family name
among the Jains.

ಪಡೀಲ್‌ padily. 7. ೩150 ಹೆಡೀಲ್‌
waste land, barrenness.

v

ಪಡ್ಯ padya. 1. ೩ spittoon.

ಪಣ್‌ panu. ೫. tosay, 10 tell, to
inform.

ಪಣ pana. #. senselessness [M].

ಪಣಕೆ panake. 1). pairing togeth-
er with a rope, ೩5 cattle.

| ಪಣಜ panaja. #.a kind of grass.

ಪಣವು papavu. 11. money in
general ; ೩ quarter rupee.

ಪಣ್ಕೆ pane. 11. contrivance for
lifting water; proper, due.

ಪಣ್ಯೆ pane. 11. quarry. Ralpape
quarry where red laterite
stones are cut.

| ಪಣ್ಯೆ pane. 1. a piece of rope tied

|

to the 110775 and neck 08
buffalos. —kutti the two
big vertical posts support-
ing the picota, nuga— yoke-

