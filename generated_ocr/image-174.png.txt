ಸಾಸ್ಯ್ಯ papu.

152

ಪಾಪ್ರ್ಯ papu. #. ೩ small foot- |
bridge across a stream.

ಪಾಪೆ pape. #. an image. pasi-
jita— the image of a pig.
ಪಾಷ್ಮೆ pape. 1. the eye- ball

ಪಾಪೊ papo. 11. sin; simplicity ;
innocence, gentleness. |
—pupyo compassion, pity. |

ಪಾಮಾಚಿ pamAci. 91. see To%,.

ಪಾಂಬಚ್ಚಿ pambacci. ೫. see ಪಾಚ್ಕಿ.

ಪಾಂಬು pambu. ೪. 10 float.

ಪಾಂಬೊಲು pambolu. #. Ribbon
fish.

ಪಾಯ paya. 7. see ಪಯ; being |
delivered of ೩ child as
among the Pariahs.

ಪಾಯಸೊ payaso. #. ೩ prepara-
tion consisting of rice, milk

and sugar.

ಪಾಯಿ payi. 1. ೩ sail.

ಪಾಯೊ payo. 11... foundation;
gain, profit.

ಪಾರ್‌ pary. ೪. 10 run, 10 fly. |

ಪಾರ್‌ 0೫೫. 7. end, extremity |
[M].

@38, para. 1. guard, watch
[pahara).

ಪಾರ್ವ para. #. brooding, hatch-
ing, incubation.

90, pari. ೫. ೩ milk-pot [M].

ಪಾರ್ಮಿ pari. 1. water [M].

ಪಾರುಪತ್ಯೊ parupatyo.
management.

%98, pare. #. hip; shoulder.

%28, pare. 7. ೩ kind of fish.

ಪಾರೆಂಗಿ parengi. ೫. an iron
crowbar used for digging;
amedium sized iron-bar
with a pointed end.

೫.

ಪಾಕೊಳು. [28801 ೫. ೩ yong 600-
11೩10.

ಪಾರ್ವಣೊ parvano. 7. ೩ parti-

cular funeral ceremony.
710. 11. ೩ share, portion.

dikka— helter-skelter.

ಪಾಲಿ pali. ೫. invoking [1],
addressing in prayer [11];
೩ vow [M].

ಪಾಲಿಪು palipu. ೪. to protect,
vouchsafe.

ಪಾಲೆ pile. #. ೩ kind of milk
tree.

name of Tuluva Brahmins.
[palyatta+-aya.]

ಪಾಲೆವು palevu. 11. ೩ big tortoise
ಗಿ].

ಪಾಲ್ಯೊ palyo. #. ೩ grant [M].

ಪಾವಣೆ pavade. 1. ೩ cloth spread
on the floor for the bride
and the bridegroom to tread
upon at ೩ wedding; girl's
garment; cloth put on the
back of a buffalo [M].

ಪಾವು pivu. 7. curing as of
tobacco.

ಪಾವುಮರ pavumara. ೫. ೩ bent
piece 08 wood over which
the receiving end of the
baropi rests.

ಪಾವೇರಿ paveri. 1೬ ೩150 ಸಾನೇಶಿ
ಪಾವೊರಿ ೩ kind 08 16601 found
in water.

ಪಾವೇಲಿ pavali. 1%. 566 ಪಾನೇರಿ.

ಪಾವೊರಿ pavori. 1. 566 ಪಾವೇರಿ,

ಪಾವೊಲಿ pavoli. 7. ೩ quarter-
Tupee. [neck.

ಪಾಶಿ 7851 ೫. hanging by the

ಪಾಲೆತ್ತ್ವಾಯ pilettaya. n. ೩ family
I
I
