ಎಡಕಾರ್‌ edakary

30

ಎಣ್ಣೆ enme

ಎಡಕಾರ್‌ edakary. ೫. left leg. !
[edattu+kary.]

ಎಡಕೈ edakai. .
[edattukai.]

ಎಡಕ್ಕುಟ್ಟ್ಸ, edakkatty. v. also |
ಜಿಕ್ಕಟ್ಟ್‌ 10 stumble; 10 stand,
to trip due 10 some obstacle.

2BYE, edakkatty. v. to stam-
mer. ayé paterunaga edak-
kattupdu there is stammer-
ing while he talks.

left hand. |

ಎಡಕೃುತ್ತರಿ edakkattari. #. 566
ಅಡಕೃತ್ತರಿ,
ಎಡಂಕ್‌' edanky. ೫. ೩೬೦ ದಂಕ್ಕ್‌

ಹಂಕ್‌ ಜೊಂಕ್‌ to stumble, 10
hit against; 10 disregard
(fg). -

ಎಡತ್‌' edaty. adv. also B,
ದತ್ತ್‌, ಯಡತ್ತ್‌ left.

ನೆಡತ್ತ್‌ edattu. 640. see ಎಡತ್‌.

ಎಡಮೆ edame. adj. ೩150 B, |
ದಮ್ಮ left.

ಎಡವಟ್ಟ್‌ edavatty. 00).  dull,
stupid. i
ಎಡವಟ್ಟ್ಟಾಯೆ edavatnayé ೫. a |
family name of Tuluva

Brahmins. .

ಎಡಸಾದಿ edasadi. #. intermedi- |
ate station in ೩ journey.
[ede+sadi.]

ಎಡುಂಬುರೆ edumbure. ೫. ೩ doll
[11].

ಎಡೆ ede. 11. 566 ಇಡೆ.

ಎಡೆಕ್ಟ್ಯಾಜಿ edekkaji. 7. also ಡಿಕ್ಕಾಜಿ
an intermediate bangle,
usually worn between two
bangles of the same variety.
(ede-+-kaji] ಸ

ಎಡೆಕ್ಳಾರ್‌ edekkary.  #.  also

@eo,0” the space between
the legs. baleny edekkaruiy
paduni 10 keep the baby
between the legs. [ede+
kar.]

ಎಡೆಕ್ಟೂಳಿ edekkili. 1. ೩150 ಜಿಕ್ಕುಲಿ
an extra 10011, in between
two regular teeth. [ede+
ka]i.]

ಎಡೆಂಕಿಲ್‌ edenkily. #. corner;
predicament (fig.).

ಎಡೆಂಜಿ edefiji. 11. also ಡೆಂಜಿ, ಜಿಂಜೈ
ಜೆಂಜಿ ೩ crab.

ಎಡೆತರೊ edetaro. 7. see ಇಡೆತಕೊ,
[ede+taro.]

ಎಡೆಮುಡಿ 60017001. 000. 566 ಇಡೆ
ಮುಡಿ.

-ಎಡ್ಡಿಲ್ಲಾಯ edkillaya. 11. ೩ family

name ೦1 Tuluva Brahmins.
edkily ?+-aya.]

ಎಷ್ಟೆ edde. 1॥. goodness; good,
excellent, prosperous, etc.

' ಎಡ್ತೆಡ್ತ್‌ edtedty. adv. forcibly,

in quick succession.
ಎಡ್ಬು edpu. ೪. 10 pound in a
mortar, to strike.

| ಎಣೆ enke. 1. thought; coun-

ting.

57, enny. ೪. to think. 10.
01111712014 manny 011011, ped-
navu poppy ಔಡ his ex-
pectations were not fulfilled,
what was born, was ೩ girl.

ಎಣ್ಣೆ enne. 1. oil.

ಎಣ್ಣೆ ಗಪ್ಪು ennegappu. 1. slightly
dark in colour ೦೯ com-
plexion. [enpe+kappu.]

SR, enpo. numeral. eighty.

ಎಣೆ enme. ೫1. gingely.
