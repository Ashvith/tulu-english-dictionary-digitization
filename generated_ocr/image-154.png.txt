ನಲಗ್‌' nalagu

ನಲಗ್‌ ಹ್‌ ೪. ಇ ೧01,
to murmur; 10 fade.

80, nali. 7. also ನಳ್ಳಿ, 3¢y, dent
as a metallic vessel.

39, nali. ». to dance. prv. nali-
pere lerivandinayague jaly
vorege he who does not
know to dance says the
floor is uneven.

ನಲಿಕೆ nalike. 2. dancing.

ನಲ್ಬೊ nalpo. 1 also ನಾಲ್ಪೊ
forty.

ನಲ್ಲ್‌ nally. 11- also 8¢% a kind
4 16೩ which infests dogs,
poultry etc; ೩ sprain.

ನಲ್ಲಿ nalli. #. the bone of the
leg below the knee.

=8 navare. 1/- a kind of grain.

ನನಸಾಗರ navasagara. 11- salt 01
ammonium.

ನನಾಯತೆ navayaté.n.a Konkani

.speaking class of Muslims |
mostly belonging to Bhat- |

1081 in 71175076 state.

ನವಿಲ್‌ 77277110, 11. also ನೆಯಿರ್ಮ a
peacock.

ನವುಂಟಿ navunta. #. itching,
irritation in the throat.

ನಸ್‌ಕ್‌' 77250100. #. ೩150 ನಸ್ವ್‌
early morning, dawn.

ನಸಿ nasi. ೫. to submit or sub-
due.

ನಸುಡಿ nasudi. 1. forehead.

ನಸೆ nase. #. moist, damp;
softness ; profit.

ನಸ್ಟ್‌ nasky. 1. see ನಸ್‌ಕ್‌.

ನಸ್ರಾಣಿ nasrani. 1. ೩ Nazarene,
a Syrian Christian ; ೩ base
mean worthless fellow [¥],

132

m"(“’) ﬂise(ﬂl)

3¢ nali. 11- 566 ನಲ್ಮಿ.

ನಳಿಗೆ nalige. 11. also ನಳ್ಳಿ, a tube,
pipe.

ನಳ 770110. 11. 566 ನಲ್ಲ್‌.

8¢y, nalli. 1. see ನಳಿಗೆ.

89y, nalli. 11. see ನಲ್ಮಿ.

ನಳ್ಳಿ nalli. 11. a kind of weed.

ನಾಕು 11110, 11. ೩ female calf [1],

ನಾಗವಂದಿಗೆ nagavandige. ೫ ೩150
ನಾಗೊಂದಿಗೆ o shelf.
ನಾಗಸಂಪಗೆ nagasampage. ೫. a
kind of tree having fragrant
flowers ol the Campaka

mﬂ"&m nllg‘m.uu n. also ನಾಗ
ಸ್ವರೂ a pipe answering to the
drone of a bag-pipe.

ನಾಗಸ್ವರೊ nagaswaro. 11. see ನಾಗ
ಸರೊ.

ನಾಗೆ ಗಗ್ಗರ. ೫. a snake, a
serpent. nagakally ೩ stone-
image of a serpent, naga-
bano a grove in which the
stone-image of a serpent is
installed and the place
considered sacred, naga-bidi
a locality affecting the
health of those dwelling
there; a poisonous growth
under water in certain
wells, drinking of whose
water is forbidden, nagara-
bella ೩ species of cane with
black spots at the joint,
010--. (6 poisonous deposit
on a betel-leaf, the chewing
of which proves fatal.

ನಾಗ್ಗ(೦") nage(ry). 1. native
Roman Catholics, so

