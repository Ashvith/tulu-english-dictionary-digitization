ತುಂಬು tumbu

116

L ತೊ 1661

ತುಂಬು tumbu. ೪. ೩150 ಸುಂಬ್ಕು | ಶೂಟಣ್‌ 11/3203. 11. ೩150 ಸೂಟನ್‌ ೩

ಹುಂಬು 10 1087, 10 carry as
೩ 10೩6.
ತುಂಬುಲಿ tumbuli. 11. 560 ತುಂಬಿಲ್‌,
ತುಂಜೆ tumbe. 11. ೩ 517811 plant
putting forth white flpwers
in the shape of a foot.

ತುಯಿ tuyi. 11- also ಸುಯಿ feather, :

quill.

ತುಯಿಂಪು tuyimpu. v. also ಸುಯಿಂ !
¥ to hiss; to grow angry; |

to blow the nose.

ತುರಕೆ turaké. 1. also ತುರ್ಕೆ a
muslim (< Turk).

ತುರುಟು. turutu. 2. a female’s
hair tied into a knot.

ತುರುಂಬು turumbu. #. chaff and
waste.

2,8 ture. 11. also ಸುರೆ a water-
gourd.

ತುರ್ಕೆ turké. 1. see ತುರುಕೆ.

ತುರ್ತು turtu. 1), swiftness, 50066.

ತುರ್ಲಿ turli. 11. obeisance.

ತುಲಾಸ್‌ (11೫೩0. ೫. ೩150 ತ್ರಾಸ್‌ ೩
balance.

ತುಲಿ tuli. 2. also 29, 10 churn.

ತುಳಸಿ tujasi. 11. the holy Basil.

ತುಳುವ tujuva. adj. pertaining
to Tulu.

ತುಳ್ಳು tuj]u. ೪. 10 hop and run
as calves.

ತುಳ್ಳೆ ಲ್‌ tullely. 7. see ಉಳ್ಳಿ ೮.

ತೂ 18. 1. ೩150 ಸೂ fire.

ತೂಕು tiku. ೪. 80 weigh; 10
feel drowsy. [lamp.

ತೂಕುಲ tukula. 2. a hanging

ತೂಕೊ 1110. #. weight.

ತೂಂಚಿ tunci. adj. also ದೂಂಚಿ
thin, fine, sharp.

;  mixture of rubbish and
earth burnt and used as
manure. [sudu--manny.]

ತೂಟಿ 116. 1. 566 ಚೂಟಿ,

| ತೂಡಿ tudi. 1. 506 ಚೂಡಿ.

| ತೂಣ 11/೩. 11. ೩150 ಬೂಣ ೩ stake,

೩ post. [Sk. 5(111೩.]

| ತೂಣಕೆರೆಂಗ್‌ tupakerengy. 1. ೩

kind of yam. [topa+

| kerengy.]

! ತೂಂಡು 1170. ೪. to aim at.

| ತೂತಿ tuti. 11. inordinate desire.

| ತೂಪು tupu. 7. winnowing as

| grain.
ತೂಂಬು tumbu. 11. also ಸೊಂಬು a
| big hole; a small pipe for

| irrigating water.

ತೂರ tura. 11. ೩ contract, parti-
cularly for toddy.
| ತೂರಿ (೫/1. 7. a kind of earthen
i vessel with ೩ long neck.
ತೂರು 1ರ. ೪. 10 purge; to
crack ೩5 ೩ ೪65561,
ತೂಲ tila.v.also ಸೂಲ್ಕಹೂಲ॥0 566,
ತೂಲಿ (11. 7. see ಚೂಲಿ,
ಶೆಕಳ್ಳು tekky. 11. 1081: wood.
ತೆಕ್ಕರೆ tekkare. 7. 566 ಶೆಕ್ಕರಿಸೆ,
ತೆಕ್ಕರ್ಪೆ tekkarpe. 11. ೯66 ಕೆಕ್ಕರಿನೆ,
ತೆಕ್ಶರ್ಮೆ tekkarme. 11. 500ಚೆಕ್ಕರ್ಮೆ,
ತೆಕ್ಳಾವು tekkavu. v. 560 ಇಕ್ಕಾವ್ರ,
ತೆರ್ಗಲ್‌ teguly. 11. ೩ 50001, ೩
bud (¢f. ಎಗ್‌ಲ್‌),
ತೆಟ್ಟ್ಸ್‌ tettu. adj. rotten, decay-
ed.
ತೆಟ್ಟ್ಸ್‌ tettu. ೪. to rot, to be-
come bad.
32, tedi. ೪. also 38, %8 to
wring hands so. as 0

