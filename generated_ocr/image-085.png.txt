ಕಿಲೆಂಬು kilembu

63

ಕುಜ್ವೆ kujve

ಕಿಲೆಂಬು kilembu. 7. 5೮೮ ಸಿಲುಂಬು, |

282 kilesi. 11. also ಕೆಲಸಿ barber. |

23, kille. 1. ೩ fort.

28, kille. #. a family name.

ತೀಚು kicu. ೪. to squeak.

ತೀಜಿ kiji. 11. white lead.

ತೀಶ್‌ kity. 11. ೩ part ; bit (as of
plantain leaves) ; a splinter. '

ಕೀತು kitu. ». also ಕೇಸು to spurt,
to sprinkle. |

ಕೀದು kidu. ೪. to importune.

ತೀಪು kipu. 7. ೩150 @€, ಸೀಪು ೩ :
bunch.

ತೀರ್‌ kiry. 11. also ಗೀರ್‌ a split,
a scratch.
ಕೀರ್ಡ್‌ kiry. ». also ಗೀರ್‌ to

|

scratch, 10 5011. |

ಕೀರ್‌ kiry. adj. past, ancient. |
kirugy melugy buddhi ೩ |
lesson for the past and the
future. |

ತೀರಿ kiri. #. continuance,
incessancy. kirikattutu barso
barpupu it rains incessantly.

ಕೀರಿಕುತ್ತು kirikuttu., ೪. see 30
ಕುತ್ತು, [kiri4-kuttu.]

ಕೀಲ್‌ kilu. 11. ೩ joint, ೩ peg.

ಕೀಲಿ 1111, 11. ೩ 100.

3, kile. 11. see ತಿಲ್ಕೆ.

58, kile. 1. see 38,

ಕೇಸ್‌ kisy. ೫. also ಗೀಸ್‌ 10 scrape
and smoothen.

ಕೀಸು kisu. ೫. see ಕೇತು,

ತೀಸುಳಿ kisuli. 11. ೩15೦ ಗೀಸುಳಿ ೩ |
carpenter’s plane used to
plane and smoothen. |

ಕೀಳ್‌ 1:1. 1. base, low. |

-9 -ku 566-5. naduku 1೬. 106
‘middle.

®39; kukka. #. name a of male
person.

ಕುಕ್ಕಿಲ್ಲಾಯ kukkilliya. #. a
family name of Tyuluva
Brahmins. [kukkillu4aya.]

ಕುಕ್ಕು kukku. . mango;{mourn-
ing feast of the Pariahs(M).

ಕುಗ್ಗು kuggu. ೫. to be humble,
to be depressed.

ಕುಂಕುಮೊ kuitkumo. #. a crim-
soned powder of turmeric.
[Sk. kunkuma.]

ಕುಂಕುರು kunkuru. #. going on
hands and legs as an
infant.

ಕುಚೋದ್ಯೊ kucsdyo. #. 1161016,
[Sk. kucodya.]

ಕುಚ್ಚಟ್‌ kuccaty. 11. shortness.

ಕುಚ್ಚಣಿಗೆ kuccanige. ೫. taunt,
upbraiding. ಇ

| ತುಚ್ಚ kucci. #. cropped hair on

‘the head ; stump of a shrub ;
stunted growth. e.g. kucci
kattuna dai plant of stunted
growth.

ಶುಜರ್‌ kujary, 1. also ಕೊಜರ್‌
stench of urine, any bad
smell.

ಕುಜಲ್‌' kujaly. ೫. also ಕೊಜಲ್ಮ್‌
the hair 01 the head.

ಕುಜಿಲಿ kujili. #. ೩ kind of
earthen vessel in which
toddy is filled.

ಕುಜುಂಬು kujumbu. ೫. ೩ chip,
fragment ; ೩ worn out stuff
(usually applied to thread-
bare clothes).

ಕುಜ್ಜಿ kujji. #. a kind of fish.

ಕುಜ್ಜೆ kujve. 1. also ಗುಣ್ಣೆ ೩ kind
