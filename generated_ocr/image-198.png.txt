ಬಾಯ್ಕಿ bayi

176

argument; to restrict the
diet; to tie the mouth of a |

corn bag, etc., —kalepu,
1181/10 vociferate, to bawl
out, —kudu to 0೮ of age,
as an animal when it- has
got its complete set of teeth,
—pata learning by heart,
—bari to be able to speak,
—budn to crack; 10 open
the mouth in wonder, to
gape, —muccilye lid of ೩
vessel, a cover, —viti taste,
appetite (from bayiruci).

ಬಾಯ್ಕಿ bayi. #. an opening.

wood, bayi. 1. edge of ೩ knife
or sword.

ಬಾಯ್ಕಿ bayi. #. ೩ Roman Catho-
lic woman.

ಬಾರ್‌ baru. 11. paddy. —palin
to let the paddy drop from

a winnower in fine stream ;

in the direction of a fav-
ourable wind 50 ೩5 10 clean
it from dust etc.

ಬಾರ್ಕ್‌ bary. ೪. 10 comb the
hair.

ಬಾರ್ಕ್‌ bary. ೫. to scrape, 10 cut
(specially wood).

ಬಾರ bara. ೫. the upper leather
strap of sandals.

ಬಾರಗೆ barage. 1. ೩ title among |

Bunt 760016.
ಬಾರಣೆ barane. 7. ೩15೦ ಬಾರ್ಣೆ
stripping the bark 01 ೩ tree;
offering of food 10 ೩ demon,
a meal after fasting.

parane ?]

K.

ಬಾರಿತ್ತ್ವಾಯ barittaya. 1. ೩ family :

ಬಾವಡ್‌ bavady

name of Tuluva Brahmins.
[barita4-aya.]

2338, bare. ೫. a kind of fish.

w28, bare. #. plantain tree.
varieties of plantains: kada-
li—, kally—, gali—, 041-
dra—, deva—, nendra—,
pii—, bopya—, maisuru—,
sifga—.

ಬಾರೆ ಸಾಂತ್‌ bare santy. ೫. ೩
rustic dance by common
folk during nights in the
month of February. [bare+
santy ?]

ಬಾರ್ಕಳ barkala. .
floor.

ಬಾರ್ಣೆ barne. 11. 5೮೮ ಬಾರಣೆ.

ಬಾರ್ದಾನೊ 0816810. ೫. ಟೀ
difference of weight in the
two scales of balance, dead
weight.

ಬಾರ್ಪಣೆ barpane. 11. ೩ comb.

ಬಾರ್ಲಿ barli. 11. barley.

ಬಾಲಡೆ 081206. 11. ೩ kind 01 fish.

ಜಾಲಪ್ಪೆ balappé. 11. 118176 01 a
male person.

ಬಾಲಸರ balasara. #. ೩ female’s
ear-ring.

ಬಾಲೆ bale. #. a child. pury—
a new-born baby, —mnu
a kind of fish.

ಬಾಲ್ಡಿ baldi. ೫. ೩ bucket. [H.
baldi.]

ಬಾವಟಿ bavate. ೫. a flag, ೩
banner.

ಬಾವದೆ bavade. 7. an earthern-
1106 or covering of a vessel.

ಬಾವಡ್‌ bavady. ೫. 10 turn
up ೩ 7106-00 after the

thrashing

