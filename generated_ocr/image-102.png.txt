-

ಕೊರಂಬೆ korambe

#5008 korambe. 7. also ಕೊಲಂಬೆ

a swamp, a marshy ground.
ಕೊರಲ್‌ koraly. #. see ಕುರಲ್‌.
ಕೊರಲ್ಮ್‌ koraly. 7. see ಕುರಲ್ಕ್‌,
ಕೊರಳೆ korale. #. cold.
ಕೊರಾಜಿ kordji. 7. 566 ಕೊರಿಜಿ,
ಕೊರ್ಕು koru. #. ೩ bar of metal.
ಕೊರ್ಕು koru. ೫. also ಕೊಳು 10

give, grant.
ಕೊರೆ kore. v. 566 ಶುರೈ.
ಕೊರೆ kore. 1. 5೮೮ ಕೊರತೆ,
ಕೊರೆಜಿ koreji. 11. ೩150 ಕೊರೊಗೋಜಿ

೩ kind of tree.
ಕೊರೆಪು korepu. v. 566 ಶುರೆಪು.
ಕೊರೊಗೋಜಿ 1: ೦೫೧85]1. ೫. see

ಕೊರೆಜಿ.
ಕೊಂರ್ಗಿನ್ಸಾಯ kornginndya. 71. ೩

family name of Tuluva

Brahmins. [kornginna -+

aya.]
ಕೊಂರ್ಗು, korigu. 7. the Indian

Beech tree.
ಕೊಂರ್ಗುನ korngu. #. crane.
ಕೊಂರ್ಗು, korngu. 7. the stalk of

cocoanut or other palm-leaf.
ಕೊಂರ್ಗುತ ಬೂರು kornguta baru. 2.

a kind of creeper.
ಕೊಂರ್ಗೆ, kornge. 11. a white cock.
ಕೊಂರ್ಗೆೈ kornge. 7. a scooped
. out wooden arrangement

with a handle for splashing

water against plants.
ಕೊರ್ನಾಯ kornaya. 1. a family
name of Tuluva Brahmins.

[korna--aya.]
ಕೊಂರ್ದ್‌ korndu. #. the stalk of

a cocoanut leaf.
ಕೊರ್ಪು korpu. 7. also ಕೊಳ್ಳು a

sprain, ೩ cramp.

8o ಕೋಚಣ್ಣೆ kocanné

ಕೊರ್ಸಂಡಿ korsandi. 11. see ಕುರ್ಸಂಡಿ.
ಕೊಲಮನಂಗ್‌ kolamanangy. 1. a

kind of 10512... [1€01೩--172೩-
nangu.]
ಕೊಲನೆ kolame. 7. a deep pit.

ಕೊಲಂಬೆ kolambe. 11. 566 ಕೊರಂಜಿ,

ಕೊಲೆಜಿ koleji. 11. Cockup fish.

ಕೊಲ್ಬು kolpu. ೫. to come into
collision, to cleave.

ಕೊಲ್ಮೆ kolme. n. delicacy, nicety.
kolmeda bele delicate work-
manship.

ಕೊಲ್ಲತರು. kollataru. 11.
Bait fish.

ಕೊಲ್ಲೆರ್‌ kollery. ೫- Jew fish.

ಕೊಳಕ್ಕ್‌ kolaku. adj. filthy,
dirty.

ಕೊಳಕ್ಕ್‌ kolaky. 11- filth, dirt.

ಕೊಳಕೆ kolake. #. third crop of
೫106,

ಕೊಳಗ 10180. 7. a 1116085076,

ಕೊಳಚೆ 101೩0. #. buffoonery;
mimicry. [bath.

ಕೊಳಂಬೆ kolambe. 11. ೩ drain, a

ಕೊಳಲ 1೭01814. 1. war, battle.

ಕೊಳವೆ kolave. 1. also ಕೊಳಾಯಿ ೩
hollow tube ; ೩ clasp.

ಕೊಳಾಯಿ kolayi. 11. see ಕೊಳವೆ.

ಕೊಳಿಕೆ kolike. ೫1. hook.

ಕೊಳು 1:0]11. ೫. 566 ಕೊರ್ಕು.

ಕೊಳೆಂಜಿ kolefiji. n. 56೮ ಕುಳೆಂಜಿ,

ಕೊಳ್ಳು kolpu. 11- 566 ಕೊರ್ಪು.

ಕೊಳ್ಳಿ 1೭0111. #. ೩ firebrand.

ಕೋಕಾಯಿ kokayi. n. 5೮ ಕೊಳ್ಳಾಯಿ.

ಕೋಂಗಣ್ಣ್‌ kongannu. #. ೩ squint
eye. [kosu+kannu.]

ಕೋಂಗಿ kongi. ೫. ridicule, jest.

ಕೋಚಣ್ಣೆ kocanné. 11. name of a
male person. [koca-+apge.]

White

