w084 kajjayo

44,

L TS

ಕಚ್ಜಾಯೊ kajjayo. 1೩. eatables |
fried in ghee or oil- usually !
sweet. in taste; pajica— ೩
sweet preparation made of
the following, five things:;
pulse, gingely seed, jaggery, |
cocoanut, honey,

¥, kajji. #.
[Sk. kharju.]

ಕಂಚಡ್ಪೆ. kaficadpe. #. a.sieve
made of bamboo, or rattan.
[kaficu+-tadpe.]

ಕಂಚಲೊ102370810.11- also ಕಂಚಾಲೊ,
ಕಂಚೋಳು the bitter-gourd.

ಕಂಚಾಲೊ 1070510. 7. 566 ಕಂಚಲೊ,,

ಕಂಚ kafici. #. gong metal ೦೯
bell metal.

ಕಂಚಿ kafici. #. ೩ place name
(Kaficipuram).

ಕೆಂಚಿಕಾಯಿ kaficikayi. 7. ೩150
ಕಂಚಿಪುಳಿ ೩. 1೦೧6 of bitter
citrus fruit used in medicine.
[kafici+kayi.]

ಂಚಿಕಾರೆ kaficikiré. 11. also ಕಂಚಿ,
ಗಾರೆ ೩ 1076 08 wasp; ೩ bell-
metal worker. [kafici-+karé.]

ಕಂಚಿಗಾರೆ kaficigaré. . see ಕಂಚಿ
ಕಾರೆ; Jkafici+-garé.] [girl[M].

ಕಂಚಿನಿ kaficini. #. ೩ dancing

ಕಂಚಿಪುಳಿ kaficipuli. 1. see ಕಂಚಿ
ಕಾಯಿ, [1:3 70-1-0011]

ಕಂಚಿಲ್‌ kaficilu. 7. ೩ vow.made
to a demon [M].

ಕಂಜೋಳ್‌: kaficoly. 566 ಕಂಚಲ್ಕೊ

ಕಂಜಲಿಗೆ kafijalige. 11. ೩ kind of
tambourine.

ಕಂಜಿ 1:೩8] 7. ೩ calf.

ಕಂಜ್ಮಿ kafiji. #. the pestle.of-a

also ಗಜ್ಜಿ itch-i

grinding stone.

ಕಂಜಿ kafiji. 11. a sheaf, of hay
or dried grass.

ಕಂಜೋಳು kafijolu. #. akindof.
bush.

ಕಂಜ್ಯ kaiijya: #. ೩ bag, or
metallic vessel, in ೫1108
pilgrims put their offerings;
wicker-work used for stor-
ing rice.

ﬁ%_ﬁ 10೩7781೩. 7.
(used for eating).

988 katale. 11. shop-keeper's
open box for putting in
cash.

ಕಟಾಕುಟಿ katakuti. 11. confusion,
disorder.

ಕಟಾರಿ ke tari. 11. ೩ dagger.

ಕಟಾರೊ. katiro. #. ೩ kind of
big vessel.

ಕಟಾವು kativu. ೩%. reaping,
harvesting.

ಕಹಬ katipiti. #. wrangling,
misunderstanding.

ಕಟುಕರೋಹಿಣಿ katukarshini. 1. ೩
kind 08 1760610170.

ಕಟುಕುಟು katukutu. 71. ೩ sound
produced 11110. 1100; or
chewing any hard sub-
stance. [katu4kutu.]

ಕಟೂರೊ katiro. adj.also ಕಟೋಕೊ'
harsh, cruel. [Sk. kathora.]

ಕಟೋರೊ katoro. 64). see ಕಟೂರೊ,

ಕಟ್ಟ್‌ katty. 2. ೩ band; ೩:11;
೩76886; bundle ೮100): an
order, a command; .--ಸಿಗ4
regulation, —Fkattale usage,
routine, —kadi. determina-
1102, settlement; —kade
final. arrangement, —kate

a plate

