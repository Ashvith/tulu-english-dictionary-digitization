~%0, -oli

40

ಒಳ್ಳೆ ಕೊಡಿ ollekodi

-ಒಲ್ಮಿ -oli. suffix. also -ಒಳ್ಳಿ ೩
suffix added to verbs mean-
ing ‘can’ or ‘may’. eg.mal-
poli can make or may make,
odoli can read or may read.

29, oli. 11- also &8 palm-leaf;
an ear ornament made by
rolling up a strip of a
palm-leaf, usually worn by
poor women.

ಒಲಿ(ಯು) oli(yu). ೪. 10 be
2168566; to ೩006೩೯ before.

. ಓಲಿಪು olipu. #. skin of ೩ snake.

%03 olipe. 1೬. gratuitous
supplies to persons 01
distinction ; ೩ gift.

_ ಓಲಿಬೆಲ್ಲೊ olibello. #. ೩150
ಓಲೆಬೆಲ್ಲೊ jaggery made of
palmyra 10106.

%033 olimade. 7. ೩ place
where devil-dancers are
painted (green-room). [oli+
made.]

ಒಲುಂಗು olungu. ೪. to move to
and fro.

_ ಒಲೆಪು olepu. v. also ಒಳೆಪ್ರ, ಲೆಪ್ಪು
10 call.

ಒಲ್ಬ 010೩, 000. ೩150 ಒಳ್ಳ, ಓಳು
where.

8, olbe. 7. ೩150 ಒಳೆ ೩ corner
region.

ಒಲ್ಲಿ olli. 7. a bed sheet; laced
cloth worn by men as upper
cloth.

| ಒಪ್ಪು ovu. pron. see ಎವು.

| w3, ovu. part. see ಅವ್ಳು. mallovu

| big ones.

ಒಸರ್‌್‌ osary. 11. ೩ spring, foun-
tain.

wX07, osary. ೪. to ooze, leak.

ಒಳಚ್ಚೈಲ್‌ olaccily. 11. also ಲಚ್ಚಿಲ್‌
೩ grove; ೩ fenced wood 768
೩ house.

| ಒಳವು 01೩೪೫. 7. ೩150 ಒಳಾವು ೩

| curve, bend; inclination,

| the mind, 86016 thought.

| ಒಳೆಸಂಚ್‌ oJasaficu. 7. 506 ಉಳಿ

| ಸಂಜಚ್‌. [ola--saficy.]

ಒಳಾವು olavu. 11. see ಒಳವು.

ಒಳ್ಳಿ 011. 11. light, 50107008.

ಒಳಿ(ಪ್ಫು oli(pu). ೨. 566 ಒರೀಿ(ಪ್ರು.

-ಓಳ್ಳಿ 011. suffix. see -ಒಲ್ಮಿ.

ಒಳೆಪು olepu. ೪. 566 ಒಲೆಪು.

ಒಳೆಯಿ 0]6]11. 11. see ಉಳಾಯಿ.

ಒಳ್ತು, oltu. 600. ೩1೨5೦ ಒಳ್ಳುಡ್ತು
from where.

ಒಳ್ಳುಡ್ರು, o]tudtu. adv. see ಒಳ್ತು.

ಒಳ್ಳೆ olpa. adv. see ಒಲ್ಬ.

ಒಳ್ಳೆ 0106. 7. 566 ಒಲ್ಬೆ.

ಒಳ್ಳೆ: 016. 7. ೩ water-snake.

ಒಳ್ಳೆ ಕೊಡಿ 0116061. #. ೩ medi-
cinal {plant. [o]le+kodi.]

