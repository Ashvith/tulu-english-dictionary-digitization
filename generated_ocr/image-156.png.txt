.

ನಾಯೊ nayo

ನಾಯೊ nayo. #. justice. [Sk.
nyaya.]

ನಾಸ್ಯ nayka. 11. one of the
community names in Tulu-
va.

ನಾರ್‌ nary. 11. fibrous matter,
fibre. —madi ೩ linen cloth.

ನಾರ್‌ nary. v. 566 ನಾದ್‌,

ನಾರಂಗಾಯಿ narangayi. ೫. ೩150
ನಾರೆಂಗಿ. lime. pro. nayigu
narangayita wppado putt-
ing lime pickle to a 608?

ನಾರಸ್ಕ್‌ 1157850. 1% thin wire;
110083 ೧8 a woman'’s nose-ring.

ನಾರಸ್ಕ್‌ narasy. adj. thin, lean.

ನಾರಸ್ಕ್‌ 157850. adj. stingy,
miserly [M].

ನಾರೆಂಗಿ narengi. 7. see ನಾರಂಗಾಯಿ,

ನಾರ್ನ narna. #. name of ೩ male
person. [Sk. narayana.]

ನಾರ್ನಪ್ಪ narnappa. #. name of
a male person. [Sk. nara-
yana.]

ನಾರ್ಲತ್ತ್ವ್ಮಾಯ narlattiya. ೫. ೩
family name of Tuluva
Brahmins. [narlatta--aya.]

ನಾಲ್‌ nalu. #. four.

ನಾಲಗೆ nalage. 7. also ನಾಲಯಿ,
ನಾಲಾಯಿ tongue. —baru 10
cease speaking as a dying
person, —maguru 10 speak
fluently, to twist state-
ments in order to suit
occasions, are— speaking
faintly ೩5 ೩ sick person, 171
— double-tongued; dupli-
city, kinni—, kiri— the
uvula.

ನಾಲಯಿ nalayi. #. 566 ನಾಲಗೆ.

134

ನಾಲಿ nali. 1). also ನಾಳಿ the throat,
windpipe. —badane a long
variety of brinjal resembl-
ing the neck.

ನಾಲೀಸ್‌' nalisy. #. dishonour,
disgrace, censure.

ನಾಲೆ 11816, 11. ೩ drain, a pipe.

ನಾಲೊ 1810. 7. ೩150 ಲಾಳೊ a
horse-shoe.

ನಾಲೊಳು naloju. 100, also ನಾಲ್ವೊ.
ಳು four times.

ನಾಲ್ಬೊ nalpo. 11. 560 ನಲ್ಬೊ.

ನಾಲ್ವೊಳು nalvolu. 000. see ನಾ
ಲೊಳು.

ನಾವಡ navada. 7. ೩ family name
among Kota Brahmins.

ನಾವಡೆ navade. 11. the claw of
a cattle.

ನಾವಳ navala. 11. four seers.

ನಾವೆ nave. 11. the central stick
attached to the bottom of
the picota bucket.

ನಾಳಿ nali. 1. see ನಾಲಿ.

ನಾಳೊ nio. 7. ೩ funnel, a tube.

- -ni. see -ಣ, 7 pateruni dala
edde 9118 your talking is no
good.

¥y niklu. pron. 500 ಇಗ್ಳು.

ನಿಗಡಿ nigadi. #. also ನೆಗಡಿ cold,
catarrh, also mucus formed
in the nose.

ನಿಗಂಟ್‌ nigantu. 11.
settlement. .

ನಿಗದಿ nigadi. #. fixing, settle-
ment; instalment.

ನಿಗಪು nigapu. ೨. also ನೆಗಪು to
erupt, to appear promi-
nently.

decision,
