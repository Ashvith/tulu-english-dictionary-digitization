ಜೊಂಬುಳಿ bombuli

185

ಜೊಳ್ಳೆ bollé

ಜೊಂಬುಳಿ bombuli. #. ೩ 7೫00 [21]. |

ಬೊಂಬೆ bombe. #. ೩ puppet, |
doll, picture. 3 !

ಜೊಂಬ್ಲಿ bombli. ೫. Bombay'
Duck fish.

ಜೊರಂಬು borambu. #. ೩ small
kind of mat.

ಜೊರಿ bori. v. also ಬೊಳಿ to milk.

ಜೊಲೆಂಜೀರ್‌ bolefijiry. 7. White
Sardine fish.

ಜೊಲ್ಯ ಮಾಂಜಿ  bolya maiiji. ೫.
Silver Promfret fish. [bolya
+mafii.]

ಜೊಳ್‌ bolu. adj. also ಜೊಳ್ಳು
white, whiteness.

ಜೊಳ್‌ಂಜಿರ್‌ bolufijirg. ೫. small,
white kind of fish.

ಜೊಳಕ್‌ bolaku. 11. fat of fish.

ಜೊಳಕು bolaku. v. see ಬಳಕ್ಕ್‌.

ಜೊಳಂಕ್‌ bolanky. 7. mire, mud.

ಬೊಳಜೆಲ್‌ bolacely. #. also ಜೊ
ಳ್ಚಟ್ಟ್‌, ಜೊಳ್ಳೆಲ್‌ paleness,
anaemic.

ಜೊಳಂತೆ bolante. ೫. 566 ಬುಳಂತೈ.

ಜೊಳಂಶೈ bojantye. 11. 506 ಬುಳಂತೈ. !

ಬೊಳಪು bolapu. #. an arrange-
ment for catching fish with
a net fringed with palm-
leaves. )

ಜೊಳಯೆ bolayé. #. also ಜೊಳಿಯೆ |
white man, an epithet used |
by Pariahs in addressing |
higher caste people.

ಜೊಳಾರ ಮಾಣಿಗೊ 018೩ manigo. |
1-:೩150 ಜೊಳ್ಳರ ಮಾಣಿಗೊ ೩ kind
08 bird. [bolira+manigo?] |

ಜೊಳಿ boli. ೪. 566 ಜೊರಿ. [

'ಜೊಳಿಯ boliya. #. name of a |
male person.

24

ಜೊಳಿಯೆ 001/36, 7. 566 ಜೊಳಯೆ.

ಬೊಳಿರ್‌ boliru. ೫. ೩ kind 08
bush.

Bpeasxn  bojumbu. 7.sap-wood
1].

ಜೊಳ್ಳಿನ್‌ bolkirg. ೫. clearing of
the sky from clouds.

ಜೊಳ್ಳಟ್ಟ್‌ bolcattu. ೫. see ಬೊಳ
ಚಿಲ್‌.

ಜೊಳ್ಬಿ bolci. 1. shyness, start-
ing aside suddenly [M].
ಜೊಳ್ಳೆ ೮ bolcely. 7. 566 ಬೊಳಜೆಲ್‌,
ಜೊಳ್ಳೊ bojco. 1೬ ೩15೦ ಜೊಳ್ಳು
light, morning.
ಜೊಳ್ಹಾತಿ boljati. 1. also ಜೊಳ್ವಾತಿ
high classed 760016, white
people. [boju+jati.]
ಜೊಳ್ಚಿ 0111. #. middle layer of
fibre of a cocoanut branch.
ಜೊಳ್ಳಿಂಗಳ್‌ boltingaly. 7. 566 ಬೊಂ.
ತೆಲ್‌, [bojy--tingalu.]
ಜೊಳ್ಣು boldu. ೧4). 566 ಬೊಳ್‌.
ಜೊಳ್ನೆಯ್‌ bolneyy. ೫. 566 ಜೊ
ಣ್ಣೆಯ್‌, [boju-+ney.]
ಜೊಳ್ಳು bolpu. 7. 566 ಜೊಳ್ಳೊ.
ಜೊಳ್ವಾ ತಿ bojvati. 1. 566 ಜೊಳ್ಳಾತಿ.
ಜೊಳ್ಳರ ಮಾಣಿಗೊ 1011೩7೩ magigo.
- 1. 566 ಬೊಳಾರ ಮಾಣಿಗೊ. [ಂ11೩-
ra-+manigo ?]
ಜೊಳ್ಳಿ, bolli. #. silver; the planet
Venus; a silver coin of a
particular value; a ‘white
animal, usually a cow or an
೦೫೭,
ಬೊಳ್ಳಿ 0111, #. name of ೩ male
person.
ಜೊಳ್ಳುಳ್ಳಿ boljulli. #. garlic.
ಜೊಳ್ಳೆ: bojlé. ೫. white-comple-
xioned bird or animal.
