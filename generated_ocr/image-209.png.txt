ಮಕೆ make

187

ಮಟ್ಟೊ macco

ಮಕೆ make. 11. the tenth lunar
asterism; religious festivals
celebrated on three auspi-
cious occasions during the
months of February and
Marchat Uppinangadi,
confluence of the sacred
rivers Netravati and Kuma-
radhara. [Sk. makha ?]

ಮಕ್ಕ makky. #. awn ೦೯ beard

of grain; irritation in the
throat.

ಮಕ್ಕಿತ್ತ್ವಾಯ makkittaya. #. ೩
family name of Tuluva
Brahmins. [makkita+aya.] |

ಮಕ್ಕೆರಿ makkeri. #. ೩ kind of |
wicker basket used 108
fishing.

Ryn°o7, magury. 06). ಬೊಂಮಗ್‌ಳ್ನ್ಮ್‌ |
next, following [M].

ಮಗ್‌ರ್ಕ್‌ magury. ೫. also ಮಗ್‌ಳ್ಮ್‌ |
relapse, 10 fall prostrate, to |
feel uneasy ೩5 in bafiji— |
to feel uneasiness in the
stomach. !

ಮುಗ್‌ರು magurpu. v. also 353,
ಮಗ್‌ಳ್ಳು, ಮಗ್ಟು 10 turn, 10
upset.

ಮಗಳ್‌ maguly. adj. see ಮಗ್‌ರ್ಮ್‌.

ಮಗ್‌ಳ್ಕ್‌ maguly. v. 566 ಮಗ್‌ರ್ಕ್‌.

ಮಗ್‌ಳ್ಪು 2780100. - 560 ಮಗ್‌ರು.

ಮಗ್ಗೆ[ಗ್ರೋಕೆ. ೫. ೩ 507,

n emagga. ೫. ೩ loom.

ಮಗ್ಗಿ maggi. 1. multiplication
table.

ಮಗ್ಗಿನೆ maggina. ೫. an obstacle,
an impediment.

SR, magte. #. second time.

ಮಗ್ಬು magpu. ೪. see ಮಗ್‌ರ್ಪು.

ಮಗ್ರೂರು magruru. ೫%. pride,
haughtiness; rudeness.

ಮಂಕ್‌ manky. #. dullness,
stupidity.

| ಮಂಕ್‌ಡಿ 17211001. #. ೩ wooden

pegin a wall to support a
7008 or a plank serving as a
shelf.

ಮಂಕಟಾವು mankativu. ೪. also
ಮಂಗಟಾವು to coax, cajole.

ಮಂಗಟಾವು mangataivu. ೪. see
ಮಂಕಟಾವು.

ಮಂಗಣೆ mangane. 1. affectation,
coquetry.

ಮಂಗಳಾರತಿ mangalarati. ೫. wav-
ing a lamp on auspicious
occasions. [Sk.]

ಮಂಗು mangu. ೫. ೩ female
cat.

oit mangé. n. ೩ monkey.
prv. manga kaity maniko
kori lekko it is ೩ 10 give
a ruby to a monkey.

ಮಗಪು magapu. ೪. also Swid, | ಮಚ್ಚರೊ maccaro. #. jealousy.

ಮೊಗಪ್ಪು to draw and turn

over.as water from ೩ tank. | ನು

ಮಗಳ್‌ magaly. #. ೩ daughter.

[Sk. matsara.]

5, macco. #. ೩ little piece
of gold or silver kept asa
