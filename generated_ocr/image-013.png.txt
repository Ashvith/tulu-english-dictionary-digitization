ix

It may be of interest to state in passing that Tulu like
other regional languages is the mother tongue of the natives
belonging to diverse castes and tribes such as the Harijans,
Jainas, Brahmins, the Bunts, the Gowdas, the Mulyas, the
Tapilyas (the oil-mongers), the Baidyas, the Christians (mostly
Protestants). This has necessarily resulted in accentuating
certain dialectical differences creating minor dialects within
itself: e.g. Brahmin Tulu, Non-Brahmin Tulu, Padri Tulu,
(according to social groups), Udupi Tulu, Karla Tulu, Puttar
Tulu, (according to geographical demarcations).

Very little of the ancient Tulu Literature is traceable
today. That is the cruel irony. As in the case of other
major languages, so for Tulu, it was the Missionary zeal that
was responsible for its revitalisation. The Basel Missionaries
who arrived in the District of South Kanara in 1834 wrote
some books in the Tulu Language and printed them ಎ the
Kannada script at the Basel Mission Press, Mangalore. The
first important among them was the publication of the Tulu
translation of the Gospel of St. Mathew (1842). Tulu
translation of the New Testament was completed in 1847 and_
«ಇ new typographical edition was issued in 1869. Other
publications intended for the Church and schools followed.”
Brigel wrote the  Grammar of the Tulu Language ೫ (1872)
in English which 10 this day 76798105 10 be the only reference
book in Tuju for students of Linguistics. In 1886 Manner
published his Tulu-English Dictionary. Its history is interesting.
The compilation of TuJu vocabulary was begun in 1856 by
Rev. G. Makkerer, who died in 1858 leaving a manuscript
containing about 2000 words. Manner added to this list
without contemplating the printing of it. However, in 1883
the printing of this Dictionary was suggested and further
steps having been taken in the matter, the then Government
of Madras was pleased to defray the cost of its publication.
In 1888 Manner's English-Tulu Dictionary was published.
Except for Tulu translation of certain other Christian ecclesias-
tical works and some of the old quasi-historical narratives
called Paddanas there is very little by way of Tulu Literature.

1t is a matter of regret that the famous * Linguistic Survey

of 88618 '' by Grierson does not take notice of Tulu. There
ii °
