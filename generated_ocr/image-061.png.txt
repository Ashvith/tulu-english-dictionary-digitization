ಒರಡೊ orado

39

ಒಲಂಪು olampu

imating to quarter of a
seer.

ಒರಡೊ orado. #. also ವರಾಡೊ |
collection, subscription.

ಒರಂಟ್‌ oranty. #. obstinacy,
stubbornness; rancour, |
hatred. |

ಒರಬೇಲ್‌' orabelu. 9. work
involving the removal of
husk from paddy.

%08 orale. 71. white ant.

ಒರಸರೆ orasare. #. ೩ kind of
sham fight.

ಒರಳ್‌ oralu. ೫1. 566 urajy.

` ಒರಾತ orata. ೫. urgency, pre- |
55076; urgent demand.

0, ori. #. ೦೧೮ man.

ಒರಿ(ಪು ori(pu). v. also ಒಳಿ(ಪು
to save, to spare; to leave |
behind, to suspend tem-
porarily.

ಒರು oru. 11. features; mark;
shade.

20, oru. #. sociability; adjust-
ability. ayagy janota oru
ijji he has no sociability
with other people.

ಒರುಂಕು orunku. 7. a very nar-
row lane between two walls;
a by-way.

ಒರುವಾರೊ oruvaro. adv. once,
throughout. [oru--varo.]

ಒರೆ ore. #. ೩ sheath.

ಒರೆಕ್ಟುಲ್‌ orekkaly. 7. see ಉರೆ
ಕೈಲ್‌, [ore-kally.]

ಒರೆಂಕ್‌' ೦೯೮೧1೦೫. ೪. also ರೆಂಕ್‌ to
2011 on the ground in the
dust or mire. ಇ

ಒರೆವು orevu. ೫. 566 ಉರೆಸ್‌,

ಒರೆಸ್‌ oresu ೪. 566 ಉರೆಸ್‌,
ಒರೊ oro. 640. once.
ಒರ್ಗು orgu. ೪. 560 ಒಗ್ಗು.

| ಒರ್ಗೊ orgo. ೫. a class; ೩ propri-

etory land; ೩ ledger book;
a transfer. [Sk. varga.]

ಒರ್ತಿ orti. 11. one woman.

ಒರ್ದ orda. 000. also ಒರ್ವ
inclined, bent.

ಒರ್ನಗ ornaga. adv. once upon
a time. [trouble.

ಒರ್ಪುಡ್ಮೆ orpude. 7. torment,

ಒರ್ಪುಡ್ಕೆ orpude. #. one side.
[or+pude.]

ಒರ್ಪೊತ್ತು, orpottu. 2. ೩150 ಒರ್ಪೊ
%, 7018 ೩ day. [or+pottu.]

ಒರ್ಪೊಳ್ಳು orpojtu. 11. see ಒರ್ಪೊತ್ತು.

ಒರ್ಬಡಿ ೦೫೩೮. adj. single. eg.
orbadi kuptn ೩ single piece
of cloth. [or+-padi ?]

| ಒರ್ಬಾಡಿ orbadi. 1. the cloth

used for rubbing floor.

ಒರ್ಮೆ orme. adv. throughout,
every-where.

ಒರ್ಮೊ ormo. #. also ಮರ್ಮೊ a
vital organ; secret (ಗ್ರೇ).
[Sk. marma.]

ಒರ್ಮ್ಟೊ ormbo. #. 3:76.

ಒರ್ಯ orya. #. remainder (cf.
ori(pu),).

ಒರ್ಲ orla. ೫. a seer equal to
80 tolas by weight.

ಒರ್ವ orva. adv. see WHF.

ಒರ್ಸೊ orso. ೫-೩ year. [Sk.varsa.]

ಒಲಂದಲ olandala. #. swoon,
fainting [M].

ಒಲಮಾಯಿ olamayi. #. pasture.

ಒಲಂಪು olampu. 1. affectation
in walking.
