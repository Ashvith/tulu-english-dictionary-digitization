ಬೇರ್‌ bary 183 ಬೊಗ್ಗುಲಿ bogguli

ಬೇರ್ಡ್‌ ಕ, ೪. to lift ಚ the | 06 village physicians. [Sk.

hand for striking; to row vaidya.]
' “a boat. 2 ಬೈಪಣೆ baipage. #. manger.
ಜೇರೆತಮರ beretamara. 7. Croton | ಬೈಪಾಡಿತ್ತಾಯ baipadittaya. 11. ೩
tree. { family name of Tuluva
ಜೇರೇಜಿ bereji. m. also ಬೇರೋಜಿ |  Brahmins.
~ amount of assessment. ಜೈಮುಗೊ baimugo. #. shyness
ಬೇಕೊ bero. 1೩ business. [Sk. [M].
vyipira.] ಬೈರಯೆ bairaye. #. also ಬೈರಿ,
ಜೇರೋಜಿ beroji. 11 566 ಬೇರೇಜಿ. ಜೈರಿಗೆ an instrument 808 bor-
ಜೇಲಿ beli. 1. hedge, fence. ing.
ಬೇಲೆ bale. #. work, labour. ಬೈರಾಗಿ bairagi. #. an ascetic.
ಬೇವು ಕಳ. 7. curry-leaf tree. ?5‘,0:7-“ bairdsy. #. an upper or
kay— neem, margosa, besa- overcloth; bath-towel.
ppu— the curry-leaf. 30 bairi. 7. 566 ಬೈರಯೆ.

ಬೇಶ 050೩, n. also ಬೇಶ್ಶ the | ಬೈರಿಗೆ bairige. 2. see ಜೈರಯೆ,
$60076 Tulu month. summer | ಬೈತಿ bairé. #. ೩ man belonging

season. [Sk. vrisabha.] to a class of Pariahs.
ಬೇಶಾಲ 154181೩, 11. hot season. ಜೈಲ್‌ baily. #. ೩ fertile kind 08
ಬೇಶ್ಯ begya. . 560 ಬೇಶ. rice-field with plenty 08
ಬೇಷ್‌ basy. 110! also ಬೇಸ್‌ good, water supply and capable
proper, excellent [F]. of two ೦೯ three crops being
ಬೇರ basy. 111. see ಬೇಷ್‌. raised.
ಜೇಸಾಯೊ besayo. #- cultivation. | ಬೊಕ್ಕಸೊ bokkaso. 7. treasure,
[Sk. vyavasiya.] treasury; large bag.
ಬೇಳೆ beje. n. split pulse. ಜೊಕ್ಕು bokku. #. mouth with-
ಬೈ bay. 7. see ಬಯಿ hay, straw. |, ೦೬ teeth.
bayta mutte hay-stack. | ಜೊಕ್ಕುಬಾಯಿ bokkubayi. #. also
ಬೈಕೋಲು baikolu. 7. the stick ಜೋಡುಬಾಯಿ mouth without
used to thrash hay. teeth.
ಜೈಂಕಿ 021010. #. name of a ಜೊಕೈ bokke. 1. 566 ಪೊಕೈೆ.
172816 person. ಜೊಕ್ಕೊ bokko. 040. 566 ಬುಕ್ಕೊ.

ಜೈತಡ್‌ baitady. 7. ೩ stick with ಜೊಗಾರಿ bogari. #. copper-smith
which hay is beaten in order |  [M].
to make it softer. ಜೊಗ್ಗಿ boggi. ೫. a bitch.

)8 baidyé. ೫. ೩ member of ಜೊಗ್ಗು boggu. #. name of a
the Billava community [ male person. , .

" “fnembers of which commu- | ಜೊಗ್ಗುಲಿ bogguli, #. Indian
pity at one time used to| squill e

