28 elle 33 .08 gt

g elle. adv. tomorrow. at seventy may well hap-

ಎವು evu. pron. also ಒಪ್ರು which. pen at seven.

7%, esalu. 7. the petal of | ಎಳ್ಳು elya. adj. small. elya 118%
a flower. a small house, madimeny

ಎಸಳ್ಳ್‌ esalu. 7. ೩ leaf. pugereta 010008 91116110 (೫೮) cel-
— tobacco leaf. ebrated the marriage on ೩

ಎಳ್ಳೊ e]po. 7. see ಎರ್ಪೊ. pru. small scale.

clpodu barpunavu eledun ಎಳ್ಳುಣ್ಣ elyapna. ೫. 566 ಎಲ್ಯಣ್ಣ.
barady what 15 10 happen [elya+-anna.]

ಎ

 é. a phoneme, peculiar to person masculine singular
Tulu, articulated at a point in all the three tenses: tinpé
slightly higher than that| he eats, 10108 he will eat,
of e. (i) realised as the | tindé he ate. (110) employ-
terminating suffix in the| ed 25 ೩ suffix in second

masculine noun or pronoun, |. person plural verbs in ಟೀ
third person, singular in the imperative mood. korlé you
nominative. Ramé Rama, give, polé you go. (iv)

magé son, ayé he. (1) | employed as ೩ particle of
employed usually as a suffix emphasis. zyé yourself, avulé
to a verb in the third | there itself.

ಏ

ನ್ಯ & a letter 08 the alphabet.| ಏಕನಾಯಕಬೂರು ೩ medicinal
&, & occasionally used as a| creeper. [eka-|nayaka.]
particle of extra emphasis. | ಏಕನಾಯಕಬೂರು ékanayakabaru.
zye you, yourself only, ini- 7. 5೮೮ ಏಕನಾಯಕ. [eka+naya-
೨8 today itself only; also ೩ ka-+buru.]
particle of interrogation, | ಏಚನೆ 50876. 1. thought. [೫೯
01108 15 it so? 608 15 it not| yocana.]
507 ಏಂಚ್‌ Eficy. ೫. 566 Do,
9, & 566 ಆೃ. kartulz 08! master. | -ಏಿಟಿ -&ta. 566 -ಎಂಗೆ. yanu 91407
'ಏಕನಾಯಕ 5102757೩10, ೫. also| mappuneta till I make this.
5
