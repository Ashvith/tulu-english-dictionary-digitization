.

ಕಡಮ್ಮೆ kadame 46

ಕಡೆಂಚಿಲ್‌ kadeficily

ಕಡಮ್ಮೆ kadame. #. female bison.
€@y, kadame. 11. deficiency.
ಕಡಮ್ಯೆ kadame. adj. 155.
ಕಡಂಬಲ್‌ kadambalu. #. the
. space hehind the hearth.
ಕಡಂಬಳಿತ್ತಾಯ kadambalittaya.
a family name of Tuluva
Brahmins. [kadambalitta-}-
‘dya.]
ಕೆಡಂಬಳೆ kadambale. 3೩ ೩ kind
of poisonous snake.
ಕಡರ kadara. 7. ೩ bride’s-maid.
ಕಡಲ್‌" kadaly. 11. the sea, ocean.
ಕಡ ® kadalusvadi. 1.
. Indian Shad fish. [kadalu+-
*svadi.] -
ಕೆಡಲೆ kadale. n. bengal gram.
933, kadavu. 2. 566 ಕಡಪ್ಪು.
ಕಡವ್ರು kadavu.'n. ೩ turning
~ lathe.
ಕಡಾಕಡಿ kadakadi. 44%. straight-
80810೩76, out-spoken, strict;
resolute.
ಕಡ್ಡಿ kadi. #-
broken rice.
%8, kadi. v. break, snap.
ಕಡಿತ್ಕೊ kadito. 1. a cut, inci- |
sion.
ಕಡಿತ್ಕೊ 1080110, 11. 5817: clu— |
, reduced 10 bones and skin. |
ಕಡಿರ kadira. 1. ೩ kind of tree.
ಕಡಿವಾಣೊ kadivano. #. the bri- |
dle of a horse. i
ಕೆಡೀರ್‌ kadiry. 00). first-born.
ಕಡು kadu. 11. pungency, excess,
a kind of poison to the fish.
ಕಡುಬು kadubu. 2. also ಕಡುಂಬು |
a kind of cake.
ಕಡುಂಬರೊ kadumbaro. #: drow-

piece. - kadiyart

siness. —pateruni to talk in
sleep:

ಕಡುಂಬು kadumbu: 11. see ಕಡುಬು,

ಕಡುಂಬೆಲೆ kadumbélé. ೫೩. miser,
೩ niggard.

ಕಡುವಾಯಿ kaduvayi. #. ೩ kind
of fish.

ಕಡುನೆ kaduvé.- 12.
courageous. man.

ಕಡ್ಕೆ kade. . to bite, to grmd
to chew.

ಕಡ್ಮೆ kade. 1. end, extremity;
place, side, direction; party.
೨-ಸಿಂಛ idanti without head
or tail, —bayi corners of
the mouth, —onduni 10
come to a decision, to reach
a safe place, —kku at last.

ಕಡ್ಕೆ kade. adj. last, final.

ಕಡೆಕಟ್ಟ್‌, kadekatty. 11. ೩ dou-
116-0055 beam.

| ಕಡೆಕಟ್ಟ್ಸ್‌ kadekatty. 11. see ಕಡ
ಕಟ್ಟ್‌, [kada-kattu.]

ಕಡೆಕಣ್ಣ್‌ kadekanny. 7. the outer
corner of the eye; a side-
glance. [kade-kanny.]

ಕಡೆಕಾರ್‌ kadekaru. 1೬ 160.
[kade--kary.]

ಕಡೆಗುರಿ kadeguri. 1. a pit used
by the end-pullers working
on ೩ water-lift. [kade-guri.]

ಕಡೆಂಚಿಲ್‌ kadeiicilu. 1. also ಅಡೆಂ.
ಚಿಲ್‌ an arrangement with
two wooden railings between
which ' naughty cows are
passed and tied to facilitate
milking  without - trouble.
೪. ane civiptuntala kadesi-
cilugy pugandy an elephant

a hero, a

