ಮುಗಿ(ಪು mugi(pu)

200

ಮುಚ್ಚಿಲ್‌ muceily

ಮುಗಿ(ಪು) mugi(pu). ೪. 10 end,
to finish. kay— 10 close
hands on salutation.

ಮುಗಿಲ್‌ mugily. 7. 566 ಮುಗಲ್‌,

ಮುಗು 2೧080. ೫. the fixed hire
with board, lodging eand
raiment of a labourer.
—tayé a servant engaged
for fixed hire, boarding, etc.

ಮುಗುಡು mugudu. #. ೩ kind of
fish.

ಮುಗುರ್ಕು muguru. ೫. also ಮುಗು
ಳು. ೩ sprout, ೩ ಉಟಗಿ. —ari
underboiled rice.

ಮುಗುರ್ಯು muguru. v. to bud, to
sprout.

ಮುಗುಳ್ಳಿ muguli.
number.

ಮುಗುಳಿ muguli. ೫. ೩ bubble.

ಮುಗುಳ್ಳಿ muguli. #. ೩ pinnacle
of a temple.

ಮುಗೆ muge. 7. also ಮೊಗೆ ೩ small
earthen vessel.

ಮುಗೆರ್‌ mugery. #. also ಮುಯೊೆರ್‌
ಮೇರ್‌ ೩ hare, ೩ rabbit.

ಮುಗೆರೆ mugere. ೫. ೩150 ಮೊಗೇರೆ
Tulu speaking fisherman
(of. ಮುಕ್ತಿ).

ಮುಗೊ mugo. ೫. ೩ demon’s
head-gear; ೩ mask. [Sk.
mukha.]

ಮುಗ್ಗಣ್‌' mugganu. 7. ೩ piece of
cloth used as underwear
1).

ಮುಗ್ಗು muggu. 7. mustiness, ೩
musty smell.

ಮುಗ್ಗೊಳಿ muggoli. ೫. ೩ kind of

. tax levied in the days of
Hyder and Tippu [M].

೫. ೩೧ odd

ಮುಗ್ರೂರು mugrdru. ೫೩. 566 ಮುಗ
ರೂರು.

ಮುಗ್ಳು 17001೪. pron. 5೮ ಇಂಬಳ್ಳು.

ಮುಂಕೆಲಿ munkeli. #. ೩ proud
woman.

ಮುಂಕೆಲೆ munkelé. 3೩ ೩ sickly
man.

ಮುಂಗ್‌ mungy. v. also ಮುರುಂ
ಕ್ಕ ಮುರುಂಗ್‌್‌, ಮುರ್ಕ್‌, ಮುಳುಂಕ್ಕ್‌
ಮುಳುಂಗ್‌ 10 sink, 10 610.

ಮುಂಗಡ mungada.n.an advance
in money.

ಮುಂಗಾರ್‌೧ಸಿ ೫೯೬. 71. ೩ hammer.

ವನಿಂಗಿಲ್‌ mungily. 1. 1877000. '

ಮುಂಗಿಲಿ mungili. ೫. ೩190 ರ್ಮುಗುಲಿ
೩ 37070056.

ಮುಂಗುಡು muingudu.
angry.

ಮುಂಗುಲಿ munguli. 11. see ಮುಂಗಿಲಿ.

ಮುಂಗೆ 77076. #. ೩ shoot, bud.

ಮುಂಗೈ 1708೩1. 1. ೩150 ಮುರಂಗೈ,
ಮನಿಳಂಗೃ, ಮೊರಂಗ್ಳೈ, ಮೊಳಂಗೈ
elbow. [mun--kai.]

ಮುಚ್ಚಂಡ್‌ muccandy. 1೩ the
part where hip joins with
the legs.

ಮುಚ್ಚಳಿಗೆ173008]180. 1. an agree-
ment in writing, deed of
settlement.

ಮುಚ್ಚಿಗೆ muccige. #. the ceiling
of a roof.

ಮುಚ್ಚಿಂತಾಯ muccintaya. ೫. a
family name of Tuluva
Brahmins. [muccinta ?+
aya.]

ಮುಚ್ಚಿ ಲ್‌ muccily. #. ೩ lid. ಶಿ)
— ೩ ೦೦೪೮5, ೩ 106, haru—
wooden lid used to cover
the mouth of a vessel

೪. 10 be
