ಮುಚ್ಚಿಲಮೆ muccilame

20%

ಮುಡ muda

from which gruel water is
sieved; it is also used as a
plate for serving rice.

armbude). adda—, kom-
bu—, neri— are some of its
varieties.

ಮುಚ್ಚಿ odmuccilame. 7. ceremo- | ಮುಟ್ಟಾಳೆ muttale. 1. 566 ಮುಟ್ಟಳೆ.

nial defilement of women .
on the third day after child- !
birth [M].

ಮುಚ್ಚು muccu. ೫. 10 shut, 10
cover. —made ೩ disguise, |
mask, covering.”

ಮುಜಜೆ mujadé. 11. an old man.

Snwoll mujanti. 11. 566 ಮಜಂಟಿ,

ಮುಜರೆ mujare. #. allowance,
subtraction, payment to be
reduced due to adjustment
of accounts.

ಮುಜು muju.s. a black monkey.

ಮುಜುಡು mujudu. 1. 87 016
person.

ಮುಜೆ muje. ೫. ೩ kind of tree
1].

ಮುಂಜಿ muifiji. #. circumcision,
initiation ceremony. [Sk.
maufiji.]

ಮುಟ್ಟ mutta. #. proximity,
nearness.

ಮುಟ್ಟಿ mutta. 000. till, until, |
as far as.

ಮುಟ್ಟಿ mutta. 1. term indicat-
ing right while driving
animals in ploughing.

ಮುಟ್ಟಿ 8, muttatti. ೫. ೩150 ಮುಟ್ಟು |
a3, ಮೊಟ್ಟಿತ್ತಿ ೩ sickle 5600866, |
to a wooden 50001 and used '
as an instrument for cutting |
vegetables. [muttu--katti.] |

ಮುಟ್ಟಳೆ muttale. . also ಮುಟ್ಟಾಳೆ |
cap made of the stalk of |
an arecanut branch (0/.

26

ಮುಟ್ಟ, mutti. . ೩ small kind of
earthen pot equal to a pint
by which toddy is sold to
customers [M].

ಮುಟ್ಟಿ mutti.n. a heavy wooden
block with a handle, used
to level court-yards.

ಮುಟ್ಟು muttu. ೪. 10 touch, to
reach.

ಮುಟ್ಟು muttu. ೫. touch, con-
tact.—kadi,—gadi a critical
state, —kattu stoppage of
urine and bowels, —cattu to
be completely defiled, poku
— to be in straitened cir-
cumstances, buddhi— to
be at one’s wit’s end.

ಮುಟ್ಟುದನೆ muttudane. ೫. 566
ಮುಟ್ಟಿತ್ತಿ.

` ಮುಟ್ಟೈ mutte. ೫. ೩150 ಮೊಟ್ಟಿ the

head ೩5 of fish ೦೯ fowls.

ಮುಟ್ಟ mutte. 2. egg.

ಮುಟ್ಟ mutte. 7. knuckles,
joints of fingers.

ಮುಟ್ಟ mutte. #. heap. baita—
a stack of straw.

ಮುಟ್ಟೊಳಿ 31101011, #. 62೭06756,
cost.

ಮುಟ್ಸ್‌ mutsy. 1. observance of
certain formulae to ward off
evil spirits and to cure
diseases.

ಮುಡೆ muda. #. stiffness, rigid-
ness; leanness. --ಸಿಂ| to
become stiff, to become lean.
