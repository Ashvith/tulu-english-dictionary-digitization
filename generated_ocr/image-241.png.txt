ಸೆಂದ್‌ sandy

ಸೆ 58. ೩ letter of the alphabet. |

ಸೆಜಂಕ್‌' sajanku. ೫. see ತಜಂಕ್‌,

ಸಕಲಾತಿ sakalati. 1೩. superior | ಸಜಪು sajapu. v. 566 ತಜಪು.
woollen stuff used 85 ೩ | ಸಜ್ಜ 58] ೩. 000. 7606/17 10೯ ೩

sheet to ೦೦೪೮೯ the body. |
ಸಕಾಯೊ sakdyo.n. easiness,
cheapness. [Sk. sahaya.]
ಸಕ್ಳೃಣೊ sakkano. 7. see ಚಕ್ಳಣ್ಕೊ.
ಸಕ್ಕರೆ sakkare. n. sugar. [Sk.
sarkara.] :
ಸಕ್ತಿ sakti. n. strength, power.
[Sk. sakti.]
ಸಗುಳೆ sagule. 1. see ಚಗುಳೆ.
ಸಂಕಟೊ sanlkato. 11 11111655, sick-
ness. [Sk. sankasta.]
ಸಂಕಪ್ಪ saikappa. #. name of
a male person. [sankara+

appa.]

ಸಂಕಬಾಳೊ sankabalo. #. ೩ kind
of serpent. [Sk. sankha-
vyala.]

ಸಂಕರ sankara. 7. name of a
male person. [Sk. ssankara.]

ಸಂಕಾರು sankaru. 1%. name of a
female person. [Sk. sankari.]

ಸಂಕು sanku. #. name of ೩ male
person.

ಸಂಕೆ sanke. #. doubt. [Sk.
ganka.]

ಸಂಕ್ಕೊ 50/1100. 11. ೩ bridge. [Sk.
581107೩17೩ ?].

ಸಂಕ್ಟೊ sanko. 1. conch. [Sk.
sankha.]

ಸಂಕೊಲೆ sankole. 11. chain, fetter.
[Sk. grinkhala.]

ಸಂಗಟಿ sangati. 7. 566 ತಂಗಟಿ,

time. [Sk. sadya.]

ಸೆಜ್ಜಿ sajji. adv. good, well,
2೦0೮೯.

ಸಜ್ಜಿಗೆ sajjige. 1. wheaten grits.

ಸಂಚ್‌' 5೩100. ೪. intrigue, plot-
ting. ula— secret plotting.

ಸೆಟ್ಟುಗೊ sattugo. 1. 566 ತಟ್ಟುಗೊ,

ಸಡಿಪು sadipu. v. 566 ತಡಿಪು.

ಸಡಿಲ್‌ sadilu. 1. 10056, slack.

ಸೆಡ್ಜ್‌ saddy. #. ೩150 ಸೊಡ್ಡು dis-
dain, inattention; indiffe-
rence.

ಸಡ್ಜ್‌ಗೆ saddugé. #. one who .
marries one’s wife’s sister.

ಸಣ್ಣ sanna. adv. small, fine.

ಸತ್ರ satra. ೫. 566 33).

ಸದ್‌ರ್‌ sadury. adj. front.

ಸಫಿಪು sanipu. v. 566 ತಣಿಪು.

ಸೆನಿಯಾರೊ saniyaro. 1 566 B
ಯಾರೊ.

ಸನೆ sane. 1. 566 3,

ಸಂತೆ sante. 94 market-place, ೩
fair.

ಸಂದ್‌ sandu. #. cleft, fissure,
opening; a joint, junction,
—katty ೩ ಜಾ, —budu to
crack, as a joint, —b@ru a
kind of medicinal plant with
visible joints or nodules at
regular intervals, —mutiu
to be polluted by an in-
direct contact.
