ಮಜ maja '
5೫77016; ೩ ಹ್‌ 6000ದೂಕ
smell [M].

ಮಜ maja. 1. also ಮಜೆ ೩ natural

speck ೦೫ spot on 176 body. |

ಮಜಕ್‌ majaky. ೪.
dim, .
Sowsoll majanti. 9. also ಮುಜಂಟ್ಕ

to become

Snwol ೩ small kind of bee;

315 wax.
ಮಜನೆ majane. ೫. ೩ kind 08
black scar on the body.
ಮಜಲ್‌ 178]810, ೫. ೩ stageina
journey; ೩ field 01 dry sort.
ಮಜಲೆ majale. ೫. rubbish,
washed on shore or bank.
ಮಜಿ maji. #. coal, charcoal,
ink. —*kalu churning stick

[M], monegu— to be dxs~|

graced.

ಮಜೂರಿ majari. #. wages [೫].

<2 maje. #. see ಮಜ.

ಮಂಚೆಲ್‌' 17870110. #. a kind of
conveyance, resembling a
hammock.

ಮಂಚೊ mafico. #. ೩ cot, bed-
stead.

S, mafifiana. ೫. name of
a male person.

ಮಂಜಪತ್ರೆ mafijapatre. ೫. ೩150
ಮಂಜಿಸತ್ರೆ ೩ medicinal plant.

[೫787]೩--56. ]

ಮಂಜಪ್ಪ mafijappa. #. name of
a male person. [mafija+
appa.]

ಮಂಜಯ್ಯ mafijayya. 9. name of
a male person. [mafija+
ayya.]

ಮಂಜಳ್‌ mafijaly. #. turmeric,
yellow. —ia adye ೩ kind of

188

ಮಟ್ಟಿ ಲ ಲ್‌ 21,

೫60ರ. on turm-
eric leaves, —kamale jaun-
dice, —tade ೩ kind of fish,
ajji—, ane—, mara— are
several kinds of turmerics.

ಮಂಜಿ maiiji. #. ೩ long boat; ೩
kind of hemp.

ಮಂಜಿಪತ್ರೆ mafijipatre. 11. 566 ಮಂ
ಜನತ್ರೆ. [maiiji+-patre.]

ಮಂಜೂರಾಯ mafijiraya. ೫. ೩
name of Tuluva Brahmins.
[mafijuru+aya.]

ಮಂಜೆ mafije. adj. barren, un-
fruitful.

ಮಂಜೆರಾಯೆ mafijerayé. 1
demon so called.

ಮಂಜೊ:788]0. #. ೩ fish-market.

ಮಂಜೊಟ್ಟಿ, mafijotti. #. the field
above the kare, where the
speed of the race-animal is
checked. —gona ೩ race
buffalo which is expected
to reach the goal (mafi-
jotti)-

' ಮಂಜೊಟ್ಟಿ mafijotti. #. also ಮಂ

ಜಟ್ಟಿ a kind of tree; the seed
of that tree (red in colour
used as a weight by gold-
smiths).

ಮಂಜೊಲು ಪಕ್ಕಿ mafijolu pakki. 1.

೩ kind 01 bird. [mafijolu+
pakki.]
iy matty. 1೬ measure,

capacity, ability, style,
manner.

ಮಟ್ಟಿರ್‌' mattary. #. a place of
cock-fighting [M].

ಮಟ್ಟಿ ಲ್‌ mattely. #. also ಮೊಟ್ಟಿಲ್‌
the pouch in front formed

ಕಾಣಾ
