ಬೊಳ್ಳೆರಿ bolleri

186

ಬ್ರಾಣಿ bragt

ಜೊಳ್ಳೆರಿ bolleri. #. ೩ kind ofl
leprosy.

ಬೊಳ್ಳೊ bollo. ೫. flood, inunda-
tion.

Bntnd bogare. adj. not quite

true, unreal [M]. ೯
ಜೋಗುಣಿ bogupi. ೫. ೩ small
sauce-pan.

ಬೋಜೊ bojo. 1೩ excellence,
greatness; pride [M].

'ಜೋಡಾದ್‌ bodady. post position
word. meaning, for the sake
of. ayagu bodady eny balte
I came for his sake.

ಜೋಡ್ಕು bodu.. ೪. to want, to
wish, to desire; to feel
necessary.

ಬೋಡ್ಕು bodu. ೫. gum without

. teeth.

ಬೋಡುಬಾಯಿ bodubayi. ೫. 566
ಜೊಕ್ಕುಬಾಯಿ. [bodu--hayi.]

ಬೋಡ್ಪಿ bodci. ೪. 566 B0k,

ಜೋಣಿ boni. #. handsel; the
first money a trader receives
in the morning. |

'ಬೋಂಟಿ bonte. #. hunting ,chase.

'ಬೋತ್ರಿ botri. v. 566 ಜೊಡ್ಬಿ.

ಜೋದಿಗೆ bodige. ೫. ೦೩1೩1 of ೩

pillar.

'ಜೋದು bodu. #. dung of an ox |
or buffalo. |

'ಬೋಜೊ bodo. 1. consciousness. |
[51 badha.] |

ಬೋನೊ bono. ೫. 18006; boiled !
rice and ೩51165 mixed into a |
paste for applying to ಟಂ |
left 5166 08 ೩ finger-drum. !
[Sk. bhojana.} !

ಜೋಮ್ಪು bombu. ೫. ೩ fire-engine;
metallic instrument for
tapping bales of rice [M].

ಜೋಯಿ boyi. ೫. also ಜೋನಿ ೩
class of fishermen who ೩೯೮
also palanquin bearers.

ಜೋರಿ bori. #. bull or ox.

ಬೋರ್ಕು boru. adj. also ಜೋಳು
bald, bare, shaven, un-
covered.

ಬೋರ್ಕು boru. ». to assault or
butt as an ox ೦೯ wild-
beast.

ಜೋರುಪೀರೆ borupire. #. ೩ kind 01
vegetable. [boru--pire.]

ಬೋರೆ bore. #. top of ೩ hill.

ಜೋರ್ಯ borya. 7. the plateau
by the side of ೩ hill.

Bt bovi. 7. see ಬೋಯಿ,

ಜೋವು bovu. 7. ೩ kind of tree,
highly valued for its fuel.

ಬೋಳಿ 1511. #. a widow whose
head has been shaven.

ಬೋಳು boju. adj. see ಜೋರ್ಕು,

ಜೋಳೆ bole. ೫. 56665 of a jack
fruit. kapputa— eye ball,
—kanpy large eyes.

ಬೋಳೊ bolo. 11. a kind of medi-
cine composed of ghee,
jaggery, cocoanut milk, etc.

ಜೋಳ್ಯಾಳ್‌ bojkaly. 7. smooth
pepper devoid of rind.
[bolu+-kalu.]

ಬ್ಯಾರಿ byari. ೫. ೩ 11001೩8; ೩
Malayalam speaking Mus-
lim. —bitty the croton-seed.
[Sk. vyapari ?]

ಬ್ರಾಣೆ brané. ೫. see ಬಿರಾಣೆ.
